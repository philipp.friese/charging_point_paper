import datetime
import itertools
from typing import Union

import pandas as pd
import numpy as np
import pytz
import cacher.cacher_class_indexed as cacher_class

from algorithms.weekdays_since import weekdays_since
from algorithms.timeframe_raster import raster_timeframes, raster_timeframes_weekday
from helperObjects.enums import NormType, IDType

def generate_occupation(df: pd.DataFrame, frequency_min: int = 10,
                        norm: NormType = NormType.NONE,
                        dt_start=datetime.datetime(2021, 5, 3),
                        dt_end=datetime.datetime.now(),
                        idType: IDType = IDType.CHARGING_PARK,
                        cacher: cacher_class.Cacher = None,
                        powerType: Union[str,None] = None,
                        filterTrx: Union[int,None] = None) -> pd.DataFrame:
    """
    For a given dataframe containing a set of charge events with a start and end timestamp, generate the occupation
    either per ID Type or for the whole dataset.

    :param df: pandas DataFrame containing charge events with start and end timestamp
    :param frequency_min: occupation frequency in minutes
    :param norm: optional normalisation type
    :param dt_start: optional start timestamp; all events prior to dt_start are removed
    :param dt_end: optional end timestamp; all events prior to dt_end are removed
    :param idType: optional ID to group by
    :param cacher: optional cacher instance; attempts to reload previously calculated values
    :param powerType: optional power type string (AC/DC) to filter dataset for
    :param filterTrx: optional amount of minimum transactions / charge events per charge point
    :returns: pandas DataFrame containing (60*24)/frequency_min rows, each representing the (potentially normalised) occupancy (per ID type)
    """

    def _generate_occupation(_df, _frequency_min: int, _norm: NormType, _idType: IDType,
                             _dt_start, _dt_end,_powerType, _filterTrx):

        _df = _df[ (_df.interval_length > datetime.timedelta(minutes=_frequency_min)) &
                   (_df.interval_length < (datetime.timedelta(hours=24) - datetime.timedelta(minutes=_frequency_min * 2)))].copy(deep=True)


        if not isinstance(_powerType, type(None)):
            print(f"Filtering for power type {_powerType}")
            _df = _df[_df.power_type == _powerType]
        _df.state_start_ts = _df.state_start_ts.apply(lambda dt: dt.astimezone(pytz.utc))
        _df.state_end_ts = _df.state_end_ts.apply(lambda dt: dt.astimezone(pytz.utc))

        if not isinstance(_filterTrx, type(None)):
            print(f"Filtering for at least {_filterTrx} trx")
            val_count = _df.evse_id.value_counts()
            val_count = val_count[val_count >= _filterTrx]
            _df = _df[_df.evse_id.isin(val_count.index)]

        df_y = raster_timeframes(_df, _frequency_min, idtype=_idType.value,
                                 start_ts_name="state_start_ts", end_ts_name="state_end_ts")

        if _norm is NormType.MAX_POSSIBLE:
            num_days = (_dt_end - _dt_start).days
            if _idType is IDType.EVSE:
                df_y /= num_days
            else:
                df_y /= float(num_days * len(_df.evse_id.unique()))

        elif _norm is NormType.RELATIVE:
            df_y /= df_y.sum() # noqa
        elif _norm is NormType.L2:
            df_y /= df_y.apply(np.linalg.norm, axis=0)
        elif norm is NormType.RESCALE:
            df_y /= df_y.max(axis=0) # noqa
        return df_y

    if not isinstance(cacher, type(None)) and cacher.shouldCache:
        return cacher.attemptCache(cacheId=f"generate_occupation-{idType}-{frequency_min}-{norm}-{dt_start}-{dt_end}-{powerType}-{filterTrx}", generate_hash=False,
                                   callback=_generate_occupation, kwargs={ "_df":df,
                                                                                    "_frequency_min":frequency_min,
                                                                                    "_norm":norm,
                                                                                    "_idType": idType,
                                                                                    "_dt_start":dt_start,"_dt_end":dt_end,
                                                                           "_powerType":powerType, "_filterTrx":filterTrx})
    else:
        return _generate_occupation(_df=df, _frequency_min=frequency_min, _norm=norm, _idType=idType,
                                    _dt_start=dt_start, _dt_end=dt_end, _powerType=powerType, _filterTrx=filterTrx)

def generate_occupation_weekday(df: pd.DataFrame, frequency_min: int = 10,
                                idType: IDType = IDType.EVSE,
                                norm: NormType = NormType.NONE,
                                dt_start = datetime.datetime(2021, 5, 3), dt_end = datetime.datetime.now(),
                                weekday_groups: list[list[int]] = None,
                                cacher: Union[cacher_class.Cacher,None] = None,
                                powerType: Union[str,None] = None) -> pd.DataFrame:
    """
    For a given dataframe containing a set of charge events with a start and end timestamp, generate the occupation
    either per ID Type or for the whole dataset.
    Additionally consider weekdays and optionally group them per weekday_group.

    :param df: pandas DataFrame containing charge events with start and end timestamp
    :param frequency_min: occupation frequency in minutes
    :param norm: optional normalisation type
    :param dt_start: optional start timestamp; all events prior to dt_start are removed
    :param dt_end: optional end timestamp; all events prior to dt_end are removed
    :param idType: optional ID to group by
    :param cacher: optional cacher instance; attempts to reload previously calculated values
    :param powerType: optional power type string (AC/DC) to filter dataset for
    :param weekday_groups: optional list of weekdays to group together (e.g. [[1,2,3,4,5],[6,7]] refers to weekday vs weekend)
    :returns: pandas DataFrame containing (60*24)/frequency_min rows, each representing the (potentially normalised) occupancy (per ID type)
    """
    def _generate_occupation_weekday(_df, _frequency_min: int = 10,
                                  _idType: IDType = IDType.EVSE,
                                  _norm: NormType = NormType.NONE,
                                  _dt_start = datetime.datetime(2021, 5, 3), _dt_end = None,
                                  _weekday_groups: list[list[int]] = None,
                                  _powerType: Union[str,None] = None) -> pd.DataFrame:

        _df = _df[(_df.interval_length > datetime.timedelta(minutes=_frequency_min)) &
                  (_df.interval_length < (datetime.timedelta(hours=24) - datetime.timedelta(minutes=_frequency_min * 2)))]

        if not isinstance(_powerType, type(None)):
            _df = _df[_df.power_type == _powerType].copy(deep=True)
        _df.state_start_ts = _df.state_start_ts.apply(lambda dt: dt.astimezone(pytz.utc))
        _df.state_end_ts = _df.state_end_ts.apply(lambda dt: dt.astimezone(pytz.utc))


        df_y = raster_timeframes_weekday(_df, _frequency_min, idtype=_idType.value,
                                         start_ts_name="state_start_ts", end_ts_name="state_end_ts")

        if isinstance(_weekday_groups, type(None)):
            _weekday_groups = [[i] for i in range(7)]


        assert set(itertools.chain(*_weekday_groups)) == {0, 1, 2, 3, 4, 5, 6}, "not all weekdays included in weekday_group!"

        group_dfs = []
        for idx, group in enumerate(_weekday_groups): # for each weekday group: add all daily occupations
            group_df = df_y.loc[(group)].reset_index().drop(columns=["weekday","time"]).groupby("daytime").sum()
            group_df["group"] = idx
            group_df = group_df.reset_index().set_index(["group", "daytime"])
            group_dfs.append(group_df)

        df_y = pd.concat(group_dfs)

        if _norm is NormType.MAX_POSSIBLE:
            num_days = np.array(weekdays_since(_dt_start, _dt_end))
            num_days = np.array([num_days[np.array(group)].sum() for group in _weekday_groups]) # sum up all passed weekdays for each group

            assert num_days.sum() == (_dt_end - _dt_start).days, \
                "num_days does not include all days that have passed since dt_start!"

            if _idType is IDType.EVSE:
                days_df = pd.DataFrame([[group_idx] + [group for _ in range(len(df_y.columns))]
                                        for group_idx, group in enumerate(num_days)],
                                       columns=["group"] + list(df_y.columns))\
                           .set_index("group")
                df_y /= days_df
            elif _idType is IDType.NONE:
                days_df = pd.DataFrame([[group_idx] + [group for _ in range(len(df_y.columns))]
                                        for group_idx, group in enumerate(num_days)],
                                       columns=["group"] + list(df_y.columns)) \
                    .set_index("group")
                days_df *= len(_df.evse_id.unique())
                df_y /= days_df
                pass
            else: raise NotImplementedError()
        elif _norm is NormType.RELATIVE:
            df_y /= df_y.groupby(level=[0]).sum() # we are dealing with a multi-index: take the sum per level-0 index (group)

        return df_y.dropna(axis=1)

    if not isinstance(cacher, type(None)) and cacher.shouldCache:
        return cacher.attemptCache(
            cacheId=f"generate_occupation_weekday-{idType}-{frequency_min}-{norm}-{dt_start}-{dt_end}"
                    f"-{weekday_groups}-{datetime.datetime.now().date()}-{powerType}",
            generate_hash=False,
            callback=_generate_occupation_weekday, kwargs={"_df": df,
                                                      "_frequency_min": frequency_min,
                                                      "_norm": norm,
                                                      "_idType": idType,
                                                      "_dt_start": dt_start,
                                                      "_dt_end": dt_end,
                                                      "_weekday_groups": weekday_groups,
                                                      "_powerType": powerType})
    else:
        return _generate_occupation_weekday(_df=df, _frequency_min=frequency_min, _idType=idType,_norm=norm,
                                            _dt_start=dt_start, _dt_end=dt_end, _weekday_groups=weekday_groups, _powerType=powerType)