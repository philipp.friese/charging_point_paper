import datetime

def weekdays_since(dt_start: datetime.datetime, dt_end: datetime.datetime = datetime.datetime.now()) -> list[int]:
    """
    Given a start and end date, how many full mondays, tuesdays, wednesdays, ... have happened between start and end?
    This information is stored in the output array at the respective weekday-offset (Monday = 0, Sunday=6)

    If start and end date do not share timezone, then this code converts end date relative to start date
    :param dt_start: start date
    :param dt_end: end date
    :return: 7-element wide list containing full days between start and end date
    """
    # for any dt_end (e.g. a thursday), get the next day _last week_ (so friday)
    # from there on, count the number of days that have passed between dt_start and last_week+i
    #   for each weekday (e.g. friday until thursday)

    if dt_start.tzinfo != dt_end.tzinfo: # always take tz of dt_start
        dt_end = dt_end.astimezone(dt_start.tzinfo)

    last_week = dt_end.replace(hour=0, minute=0, second=0, microsecond=0)
    last_week -= datetime.timedelta(days=6, microseconds=1)
    num_days = [(
        (last_week + datetime.timedelta(days=i)).weekday(), # keep track of weekday for sorting
        (last_week + datetime.timedelta(days=i) - dt_start).days // 7 + 1
    ) for i in range(7)]

    num_days = sorted(num_days, key=lambda x: x[0]) # sort array based on weekday, s.t. monday (0) comes first

    return list(map(lambda x: x[1], num_days)) # extract actual num_days component at tuple index 1