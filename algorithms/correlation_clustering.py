import cacher.cacher_class_indexed as cacher_class
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist

from helperObjects.classes import MergeRecord, ClusterResult
from helperObjects.enums import MergeType

def cluster(occupation: pd.DataFrame, chargings_df: pd.DataFrame, corr_threshold: float, mergeType: MergeType,
            cacher: cacher_class.Cacher = None) -> ClusterResult:
    """
    Given:
        - a DataFrame containing rastered & normalised event-counts, where each column represents one location ID and each row the relative amount of events per time bucket (also referred to as occupancy curves)
        - a DataFrame containing the total number of charging events per location ID
        - a correlation threshold 0 <= corr_threshold <= 1
        - a merge policy [IDS or CHARGINGS]
    Use agglomerative clustering to compute clustered occupancy curves. This implements the following approach:
        - caluclate the correlation distance between all occupancy curves
        - choose the curve pair with maximum correlation / minimal correlation distance
        - merve curve pair, remove old curves and add new merged curve to curve set
        - repeat until maximum observed correlation falls below corr_threshold


    :param occupation: DataFrame containing Occupancy Curves per location ID
    :param chargings_df: DataFrame containing number of charging events per location ID
    :param corr_threshold: correlation threshold; stop iterations if maximum observed correlation falls below threshold
    :param mergeType: merge policy for occupancy curve merging
    :param cacher: optional cacher instance; attempts to reload previously calculated values
    :return: ClusterResult, containing merged curve values, the contributing location IDs per merged curve and history of maximum observed correlation
    """

    def _merge_curves(values: np.array, row: int, col: int, merge_record: list[MergeRecord], _mergeType: MergeType) -> tuple[np.array, list[MergeRecord]]:
        curve_row = values[row]
        curve_col = values[col]

        # add both curves, weighted by how many times they have been merged previously
        # normalize using sum of both merge amounts
        if _mergeType is MergeType.IDS: # ids are stored as set, so take their length to get number of merged ids per curve
            new_curve = ((curve_col * len(merge_record[col].ids)) + (curve_row * len(merge_record[row].ids))) \
                        / float(len(merge_record[row].ids | merge_record[col].ids))
        elif _mergeType is MergeType.CHARGINGS:
            new_curve = ((curve_col * merge_record[col].trx) + (curve_row * merge_record[row].trx)) \
                        / float(merge_record[row].trx + merge_record[col].trx)
        else: raise NotImplementedError(f"Merge Type {_mergeType.value} not implemented!")

        # update merge_record & values by overwriting data for row
        merge_record[row] = merge_record[row] + merge_record[col]
        values[row] = new_curve

        # delete data for col
        values = np.delete(values, col, axis=0)
        merge_record.pop(col)

        return values, merge_record

    def _cluster(_occupation, _corr_threshold, _chargings_df, _mergeType: MergeType) -> ClusterResult:
        occupation_T = _occupation.T
        values = occupation_T.values
        _ids = _occupation.columns.values

        # keep track on how many times one row has been merged already
        #  using either the number of merged ids (tuple index 0) or the number of chargings at said id (tuple index 1)
        merged_ids = pd.DataFrame(_ids,columns=["id"]).merge(_chargings_df[["id","num_chargings"]],on="id",how="left")
        merge_record = [MergeRecord({t[0]},t[1]) for t in merged_ids.values]
        assert len(merge_record) == len(_ids)

        max_cor = 1
        correlations = []

        c = 0
        while max_cor > _corr_threshold:
            print(f"\r[{c:>3}] current maximum similarity: {round(max_cor,4):>6}", end="")
            c += 1

            mat = cdist(values, values, metric="correlation")
            mat = 1-mat # 'correlation' metric returns correlation distance (1-correlation), but we need the correlation (1-corr_dist)
            np.fill_diagonal(mat, 0) # replace diagonals with 0; each curve is identical to itself, which would mess with the following argmax-es

            # find curve pair with best correlation
            row_with_max_cor = int(np.nanargmax(np.nanmax(mat, axis=1)))
            col_with_max_cor = int(np.nanargmax(mat[row_with_max_cor]))
            max_cor = float(np.nanmax(mat[row_with_max_cor]))
            correlations.append(max_cor)
            if max_cor > _corr_threshold:
                values, merge_record = _merge_curves(values, row_with_max_cor, col_with_max_cor, merge_record, _mergeType)
            else:
                break
        print()
        return ClusterResult(values, merge_record, correlations)

    if isinstance(cacher, cacher_class.Cacher) and cacher.shouldCache:
        return cacher.attemptCache(cacheId="occupation-cluster", generate_hash=True,callback=_cluster,
                                   kwargs={"_occupation": occupation, "_corr_threshold": corr_threshold,
                                           "_chargings_df": chargings_df, "_mergeType":mergeType})
    else:
        return _cluster(_occupation=occupation, _corr_threshold=corr_threshold,
                        _chargings_df=chargings_df, _mergeType=mergeType)