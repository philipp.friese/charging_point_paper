import pandas as pd
import numpy as np
import datetime
from typing import Union

def _simple(_df: pd.DataFrame, df_idx: list[Union[datetime.time,datetime.datetime]], raster_size: int) -> pd.DataFrame:
    start_end_pairs = _df[["start_idx", "end_idx", "wrap"]].values

    yvals = np.zeros(raster_size,dtype=np.int)
    for start_idx, end_idx, wrap in start_end_pairs:
        vals = np.zeros(raster_size,dtype=np.int)
        vals[end_idx] = -1
        vals[0] = wrap
        vals[start_idx] = 1
        yvals += np.cumsum(vals).astype(np.int)

    raster_df = pd.DataFrame(yvals.astype(int),columns=["count"])
    raster_df.index = df_idx
    return raster_df


def _subtyped(_df: pd.DataFrame, df_idx: list[Union[datetime.time,datetime.datetime]],
              raster_size: int, idtype_str: str) -> pd.DataFrame:
    subtypes = _df[idtype_str].unique()
    start_end_pairs = _df[[idtype_str, "wrap", "start_idx", "end_idx"]].values

    yvals = {subtype: np.zeros(raster_size,dtype=np.int) for subtype in subtypes}
    for i, (idtype_str, wrap, start_idx, end_idx) in enumerate(start_end_pairs):
        vals = np.zeros(raster_size,dtype=np.int)
        vals[end_idx] = -1
        vals[0] = wrap
        vals[start_idx] = 1
        yvals[idtype_str] += np.cumsum(vals).astype(np.int)

    raster_df = pd.DataFrame(yvals)
    raster_df.index = df_idx

    return raster_df


def raster_timeframes(df: pd.DataFrame, resolution_min: int, idtype: Union[str, None] = None,
                      start_ts_name: str = "start_ts", end_ts_name: str ="end_ts") -> pd.DataFrame:
    """
    Given a DataFrame containing the start and end datetime of an event (configurable with {start,end}_ts_name) with an optional location ID (stored in idtype),
    as well as a raster resolution in minutes and an ID, calculate the number of events occurring within one resolution-sized day-time bucket.
    Example:
        For a resolution of 10 minutes, have time buckets [0h0min, 0h10min, ..., 23h40min, 23h50min].
        Have events e1 from day x 9AM-11:30AM and e2 from day y 8:30AM-10AM.
        Time bucket 9h0min will count two events, time bucket 8h40min one event and time bucket 7h50min zero events.

    This algorithm requires all events to be at most 24h-2*resolution_min long.

    :param df: DataFrame containing events from star_ts_name to end_ts_name; if idtype!=None, df must contain a column idtype with event location IDs
    :param resolution_min: raster resolution in minutes
    :param idtype: optional location ID
    :param start_ts_name: DataFrame column containing start datetime timestamp
    :param end_ts_name: DataFrame column containing end datetime timestamp
    :return: DataFrame with index "time" (containing all resolution_min-sized buckets) and either column "count" if idtype==None or columns df[idtype].unique()
    """

    assert ((60 * 24) / resolution_min).is_integer(), "resolution_min must tile 1440min"
    assert ((df[end_ts_name] - df[start_ts_name]) < (datetime.timedelta(days=1) - datetime.timedelta(minutes=resolution_min *2))) \
           .all(), "maximum allowed timeframe length is 24h-2*resolution_min"
    assert ((df[end_ts_name] - df[start_ts_name]) > (
        datetime.timedelta(minutes=resolution_min))) \
        .all(), "minimum allowed timeframe length is 2*resolution_min"

    num_slices = (60 * 24) // resolution_min
    df_idx = [(datetime.datetime(2021, 1, 1, 0, 0, 0)
                               + datetime.timedelta(minutes=resolution_min * x)).time()
                              for x in list(range(num_slices))]

    df["start_idx"] = pd.to_numeric(np.floor((  df[start_ts_name].dt.hour * 60
                                              + df[start_ts_name].dt.minute
                                              + df[start_ts_name].dt.second / 60)
                                             / resolution_min),
                                    downcast="integer")
    df["end_idx"]   = pd.to_numeric(np.floor((  df[end_ts_name].dt.hour * 60
                                              + df[end_ts_name].dt.minute
                                              + df[end_ts_name].dt.second / 60)
                                             / resolution_min),
                                    downcast="integer")

    # check if interval wraps around midnight
    df["wrap"] = (df[end_ts_name].dt.time < df[start_ts_name].dt.time) & (df.end_idx != 0)

    if isinstance(idtype, type(None)):
        raster_df = _simple(df, df_idx, num_slices)
    else:
        raster_df = _subtyped(df, df_idx, num_slices, idtype)

    return raster_df

def raster_timeframes_weekday(df: pd.DataFrame, resolution_min: int, idtype: Union[str, None] = None,
                              start_ts_name: str = "start_ts", end_ts_name: str ="end_ts") -> pd.DataFrame:
    """
    Given a DataFrame containing the start and end datetime of an event (configurable with {start,end}_ts_name) with an optional location ID (stored in idtype),
    as well as a raster resolution in minutes and an ID, calculate the number of events occurring within one resolution-sized time bucket, per weekday.
    Example:
        For a resolution of 10 minutes, have time buckets [0h0min, 0h10min, ..., 23h40min, 23h50min].
        Have events e1 from day x 9AM-11:30AM and e2 from day y 8:30AM-10AM.
        Time bucket 9h0min will count two events, time bucket 8h40min one event and time bucket 7h50min zero events.

    :param df: DataFrame containing events from star_ts_name to end_ts_name; if idtype!=None, df must contain a column idtype with event location IDs
    :param resolution_min: raster resolution in minutes
    :param idtype: optional location ID
    :param start_ts_name: DataFrame column containing start datetime timestamp
    :param end_ts_name: DataFrame column containing end datetime timestamp
    :return: DataFrame with indices "weekday" and "daytime" (containing all resolution_min-sized day-time buckets, per weekday) and either column "count" if idtype==None or columns df[idtype].unique()
    """
    assert ((60 * 24) / resolution_min).is_integer(), "resolution_min must tile 1440min"
    assert ((df[end_ts_name] - df[start_ts_name]) < (datetime.timedelta(days=1) - datetime.timedelta(minutes=resolution_min *2))) \
           .all(), "maximum allowed timeframe length is 24h-2*resolution_min"
    assert ((df[end_ts_name] - df[start_ts_name]) > (
               datetime.timedelta(minutes=resolution_min))) \
        .all(), "minimum allowed timeframe length is 2*resolution_min"

    num_slices_per_day = (60 * 24) // resolution_min
    num_slices = num_slices_per_day * 7
    df_idx = [(datetime.datetime(2021, 1, 1, 0, 0, 0)
                               + datetime.timedelta(minutes=resolution_min * x))
                              for x in list(range(num_slices))]

    df["weekday"] = df.state_start_ts.dt.weekday
    df.sort_values("weekday", inplace=True)
    df["start_idx"] = pd.to_numeric((num_slices_per_day * df[start_ts_name].dt.weekday)
                                    + np.floor((  df[start_ts_name].dt.hour * 60
                                                + df[start_ts_name].dt.minute
                                                + df[start_ts_name].dt.second / 60)
                                               / resolution_min) % num_slices,
                                    downcast="integer")

    df["end_idx"] = pd.to_numeric((num_slices_per_day * df[end_ts_name].dt.weekday)
                                   + np.floor((  df[end_ts_name].dt.hour * 60
                                               + df[end_ts_name].dt.minute
                                               + df[end_ts_name].dt.second / 60)
                                              / resolution_min) % num_slices,
                                   downcast="integer")

    # check if interval wraps around midnight, from sunday (6) to monday (0); other wraps are fine
    df["wrap"] =   (df.state_end_ts.dt.time < df.state_start_ts.dt.time) \
                 & (df.state_start_ts.dt.weekday == 6) \
                 & (df.state_end_ts.dt.weekday   == 0) \
                 & (df.end_idx != 0)

    if isinstance(idtype, type(None)):
        raster_df = _simple(df, df_idx, num_slices)
    else:
        raster_df = _subtyped(df, df_idx, num_slices, idtype)

    raster_df = raster_df.reset_index().rename(columns={"index":"time"})
    raster_df["weekday"] = raster_df.time.dt.weekday
    raster_df["daytime"] = raster_df.time.dt.time
    raster_df.set_index(["weekday","daytime"], inplace=True)
    return raster_df
