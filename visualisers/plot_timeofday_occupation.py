import calendar
from typing import Union
import os,sys
from pathlib import Path

import datetime

import argparse
import pandas as pd
import numpy as np
import matplotlib.ticker
import matplotlib.dates
import matplotlib.pyplot as plt
import matplotlib.cm
import tempfile
from matplotlib.colors import Normalize
from mpl_toolkits.axes_grid1 import make_axes_locatable

from cacher import cacher_class_indexed as cacher_class

from aggregators.occupation import generate_occupation_weekday, generate_occupation
from helperObjects.enums import NormType, IDType

pd.options.display.width = 1920
pd.options.display.max_columns = 144
pd.options.display.max_rows = 100
pd.options.display.min_rows = 100

import logging
log = logging.getLogger(__name__)
__format = "{filename}:{lineno} {asctime} {levelname[0]} - {message} "
__style = '{'
__level = logging.INFO
logging.basicConfig(format=__format, style=__style, stream=sys.stdout, level=__level) # noqa

def plot_occupation_acdc(df_y: pd.DataFrame, basepath: Path, frequency_min: int = 10,
                         title: bool = True, savefig: bool = False,
                         figsize: tuple[float,float] = (19,10),
                         fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising Occupancy for AC and DC charge stations")
    fig, ax = plt.subplots(figsize=figsize)
    ax: plt.Axes
    fig: plt.Figure

    num_slices = (60*24)//frequency_min
    xticks = list(range(num_slices))
    xticks_dt = [(datetime.datetime(2021, 1, 1, 0, 0, 0) + datetime.timedelta(minutes=frequency_min * x))
                 for x in xticks]

    colors = ["tab:blue","tab:red"]
    for color, powertype in zip(colors, ["AC","DC"]):
        y = df_y[powertype]
        ax.plot(xticks_dt, y, label=powertype, color=color)

    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(1.0, decimals=0))
    ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator([round(i,2) for i in np.arange(0,df_y.max().max()*1.1,0.02)])) # noqa
    ax.yaxis.set_minor_locator(matplotlib.ticker.FixedLocator([round(i,2) for i in np.arange(0,df_y.max().max()*1.1,0.01)])) # noqa


    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_major_locator(matplotlib.dates.HourLocator(byhour=range(0, 24)))


    ax.xaxis.grid(True)
    ax.xaxis.grid(True, which="minor")
    ax.yaxis.grid(True)

    ax.set_xlim(datetime.datetime(2021,1,1), datetime.datetime(2021,1,2)) # noqa
    ax.set_xlabel(f"Time of Day (UTC)")

    ax.set_ylim(0,df_y.max().max()*1.1) # noqa
    ax.xaxis.set_tick_params(rotation=90)
    ax.set_ylabel(f"Occupancy in %")
    if title:
        ax.set_title(f"Occupancy of all observed charge points\nData from 2021-05-01 to {datetime.datetime.now().date()}, raster size: {frequency_min}min")

    plt.legend()
    plt.tight_layout()
    if savefig:
        fp = basepath / "occupation-all.pdf"
        plt.savefig(fp, dpi=300)
        log.info(f"[Figure {fig_num}] File was written to: {fp}")
    else:
        plt.show()



def plot_occupation_week_im(df_final,  powerType: Union[str,None],  basepath: Path, frequency_min: int = 10,
                            interpolation: str = 'antialiased', title: bool = True,
                            figsize: tuple[float,float] = (19,10), savefig: bool = False,
                            file_ending: str = "pdf", fig_num: Union[int,str] = "X"):

    num_slices_per_day = (60 * 24) // frequency_min
    fig, ax = plt.subplots(figsize=figsize)
    ax: plt.Axes
    fig: plt.Figure

    norm = matplotlib.colors.Normalize(vmin=round(df_final.min().min(),2), vmax=round(df_final.max().max(),2))
    im = ax.imshow(df_final, aspect="auto",
                   cmap="gist_heat", norm=norm,
                   interpolation=interpolation)
    for i in range(-1,7):
        ax.axvline(i+.5, color="black", linewidth=3)

    xticks = range(7)
    ax.set_xticks(xticks)
    ax.set_xticklabels(list(calendar.day_name))

    yticks = list(range(num_slices_per_day))

    ylabels = [ (datetime.datetime(2021, 1, 4, 0, 0, 0)
                 + datetime.timedelta(minutes=frequency_min * x)).time().__str__()[:-3]
               for x in yticks]
    num_filter = 6*5
    ax.set_yticks(yticks[::num_filter])
    ax.set_yticklabels(ylabels[::num_filter])
    ax.set_ylabel(f"Time of day (UTC)")

    if title:
        ax.set_xlabel(f"Occupancy per weekday{', ' + powerType + ' charge points' if powerType else ''}")
        ax.set_title(f"Occupancy of all observed charge points, per weekday\nData from 2021-05-01 to {datetime.datetime.now().date()}, "
                     f"raster size: {frequency_min}min, "
                     f"interpolation: {interpolation}")

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im, cax=cax, label="Occupancy in %",
                 ticks=[round(i,2) for i in np.arange(df_final.min().min(), df_final.max().max()+0.01, 0.01)],
                 format=matplotlib.ticker.PercentFormatter(1.0, decimals=0))
    plt.tight_layout()

    if savefig:
        fp = basepath / f"occupation_week_im-{powerType if powerType else 'all'}_{interpolation}.{file_ending}"
        plt.savefig(fp,dpi=300)
        log.info(f"[Figure {fig_num}] File was written to: {fp}")
    else:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", help="path to folder containing 'dataset.csv' (default: $PROJECT_ROOT/data)", type=str,
                        default=Path(os.path.abspath(__file__)).parent.parent / "data") # file is one folders in; so call .parent twice
    parser.add_argument("--outpath", help="path to image output (default: temporary directory)", type=str, default=None)
    args = parser.parse_args()

    if outpath := args.outpath:
        basepath_output = Path(outpath)
    else:
        basepath_output = Path(tempfile.gettempdir()) / "images"
        log.info(f"Outpath not provided, will use a temporary folder at: {basepath_output}")
    if not basepath_output.is_dir():
        log.info(f"Folder at: {basepath_output} does not exist, will try to create!")
        os.mkdir(basepath_output)


    _cacher = cacher_class.Cacher(maxCacheAge=datetime.timedelta(hours=8))
    _normType = NormType.MAX_POSSIBLE
    _idType = IDType.NONE
    _frequency_min = 2
    _filterTrx = 10

    _dt_start = datetime.datetime(2021, 5, 6)
    _dt_end = datetime.datetime(2021, 10, 6)

    _figsize = (19,7)

    plt.rc('font', size=16)  # controls default text size
    plt.rc('axes', titlesize=16)  # fontsize of the title
    plt.rc('axes', labelsize=16)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=16)  # fontsize of the x tick labels
    plt.rc('ytick', labelsize=16)  # fontsize of the y tick labels
    plt.rc('legend', fontsize=16)  # fontsize of the legend
    _title = False
    _savefig = True

    for folder in ["images","analyses"]:
        if not (Path("/tmp") / folder).is_dir():
            os.mkdir(Path("/tmp") / folder)

    dfs = []
    dfs_week = []
    df_paper = pd.read_csv(Path(args.dataset) / "dataset.csv",sep=";",compression="gzip",
                     parse_dates=["state_start_ts","state_end_ts", "interval_length"])
    df_paper["interval_length"] = pd.to_timedelta(df_paper.interval_length)
    for powerType in ["AC", "DC"]:
        # daytime
        _df_y = generate_occupation(df=df_paper, frequency_min=_frequency_min, norm=NormType.MAX_POSSIBLE,
                                   dt_start=_dt_start, dt_end=_dt_end,
                                   idType=_idType, cacher=_cacher, powerType=powerType, filterTrx=_filterTrx)
        dfs.append(_df_y)

        # weekday
        df_final = generate_occupation_weekday(df=df_paper, frequency_min=_frequency_min,
                                               dt_start=_dt_start, dt_end=_dt_end,
                                               norm=NormType.MAX_POSSIBLE, idType=_idType, cacher=_cacher, powerType=powerType)
        df_final = df_final \
            .reset_index() \
            .pivot(index="daytime", values="count", columns="group") \
            .rename(columns={i: c for i, c in enumerate(calendar.day_name)})
        dfs_week.append(df_final)
    df_acdc = dfs[0].join(dfs[1], rsuffix="_dc").rename(columns={"count": "AC", "count_dc": "DC"})

    # Figure 7
    plot_occupation_acdc(df_y=df_acdc,
                         frequency_min=_frequency_min,
                         title=_title, savefig=_savefig,
                         figsize=_figsize,
                         basepath=basepath_output, fig_num=7)

    # Figure 8
    for df_week, powerType in zip(dfs_week, ["AC", "DC"]):
        plot_occupation_week_im(df_week, frequency_min=_frequency_min,
                                interpolation='none',
                                title=_title, savefig=_savefig,
                                figsize=_figsize, powerType=powerType,
                                file_ending="png",
                                basepath=basepath_output, fig_num=8)
