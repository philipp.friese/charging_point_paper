import datetime, os,sys

import argparse
import matplotlib.dates
import matplotlib.pyplot as plt
import matplotlib.ticker
import pandas as pd
import tempfile
from matplotlib.lines import Line2D
from pathlib import Path
from typing import Union

from aggregators.occupation import generate_occupation
from analysers.occupation_clustering import cluster_ac_dc
from helperObjects.classes import ClusterResult
from helperObjects.enums import MergeType, IDType, NormType

import cacher.cacher_class_indexed as cacher_class

import logging
log = logging.getLogger(__name__)
__format = "{filename}:{lineno} {asctime} {levelname[0]} - {message} "
__style = '{'
__level = logging.INFO
logging.basicConfig(format=__format, style=__style, stream=sys.stdout, level=__level) # noqa


def plot_clusters_ac(data: ClusterResult,
                       frequency_min: int, corr_threshold: float, occupation: pd.DataFrame, idType: IDType, mergeType: MergeType,
                     basepath: Path, cluster_colors: dict[int,str],
                       savefig: bool = False, figsize: tuple[int,int] = (19,11), curves_per_group=3,
                       title: bool = False, fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising AC clusters, per cluster")
    num_slices = (60 * 24) // frequency_min
    xticks = list(range(num_slices))
    xlabels = [(datetime.datetime(2021, 1, 1, 0, 0, 0) + datetime.timedelta(minutes=frequency_min * x))
               for x in xticks]


    plotted_ids = set()
    occupation_T = occupation.T


    result: ClusterResult
    max_trx = sum(list(map(lambda x: x.trx, data.merge_records)))
    max_ids = sum(list(map(lambda x: len(x.ids), data.merge_records)))
    top_curves, top_merge_records = data.filter_top_n(n=curves_per_group, mergeType=mergeType)
    for j in range(curves_per_group):
        fig, ax = plt.subplots(figsize=figsize)
        fig: plt.Figure
        ax: plt.Axes
        merge_entry = top_merge_records[j]
        curve = top_curves[j]

        plotted_ids |= set(merge_entry.ids)
        color = cluster_colors.get(j, "grey")
        ax.plot(xlabels, curve, color=color,linewidth=3, label="Merged Occupancy Curve")
        perc_trx = round(merge_entry.trx/max_trx * 100, 1)
        perc_ids = round(len(merge_entry.ids)/max_ids * 100, 1)


        for _i, idx in enumerate(merge_entry.ids):
            y = occupation_T.loc[idx].values
            ax.plot(xlabels, y, alpha=0.03, color=color, zorder=5)

        # axis formatting & labeling
        ax.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:.4f}'))
        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(10))

        ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
        ax.xaxis.set_major_locator(matplotlib.dates.HourLocator(byhour=[0, 6, 12, 18]))
        ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%H:%M'))
        ax.xaxis.set_minor_locator(matplotlib.dates.HourLocator(byhour=range(0, 24, 1)))
        ax.xaxis.set_tick_params(rotation=90)
        ax.xaxis.set_tick_params(rotation=90, which="minor")
        top = max([occupation_T.quantile(.97).quantile(.97),
                   max(curve)]) # noqa
        ax.set_ylim(bottom=0, top=top)
        ax.set_ylabel(f"Occupancy Density")
        ax.set_xlabel(f"Time of Day (UTC)")

        handles,_ = ax.get_legend_handles_labels()
        line = Line2D([0], [0], label='Charge Point Occupancy Curves', color=color, linewidth=1)
        handles.extend([line])
        ax.legend(handles=handles)

        ax.set_xlim(datetime.datetime(2021, 1, 1, 0, 0, 0),datetime.datetime(2021, 1, 2, 0, 0, 0)) # noqa
        title_str = f"Clustered Occupation Curves, by {idType.value} ({len(plotted_ids)} {idType.value}s)\n" \
                    f"raster resolution: {frequency_min}min, correlation threshold: {corr_threshold}, " \
                    f"merge type: {mergeType.value}, data until {datetime.datetime.now().date()}"
        if title:
            ax.set_title(f"cluster {j + 1} - {len(merge_entry.ids)} charge points ({perc_ids}%), "
                         f"{int(merge_entry.trx)} charge events ({perc_trx}%)")
            fig.suptitle(title_str, fontsize=18)
        else:
            print(f"cluster {j + 1} - {len(merge_entry.ids)} charge points (${perc_ids}\%$), "
                     f"{int(merge_entry.trx)} charge events (${perc_trx}\%$)")

        fig.tight_layout()
        if savefig:
            fig.subplots_adjust(top=0.94)
            fp = basepath / f"clusters_ac-id-{j+1}.png"
            plt.savefig(fp, dpi=300)
            log.info(f"[Figure {fig_num}] File was written to: {fp}")
        else:
            plt.show()


def plot_clusters_ac_all(data: ClusterResult,
                       frequency_min: int, corr_threshold: float, idType: IDType, mergeType: MergeType,
                         basepath: Path, cluster_colors: dict[int,str],
                       savefig: bool = False, figsize: tuple[int,int] = (19,11), curves_per_group=3,
                       title: bool = False, fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising all AC clusters")
    num_slices = (60 * 24) // frequency_min
    xticks = list(range(num_slices))
    xlabels = [(datetime.datetime(2021, 1, 1, 0, 0, 0) + datetime.timedelta(minutes=frequency_min * x))
               for x in xticks]

    plotted_ids = set()

    fig, ax = plt.subplots(figsize=figsize)
    fig: plt.Figure
    result: ClusterResult
    top_curves, top_merge_records = data.filter_top_n(n=curves_per_group, mergeType=mergeType)
    for j in range(curves_per_group):
        merge_entry = top_merge_records[j]
        curve = top_curves[j]
        plotted_ids |= set(merge_entry.ids)
        color = cluster_colors.get(j, "grey")
        ax.plot(xlabels, curve, color=color,linewidth=2, label=f"Cluster {j+1}")

    # axis formatting & labeling
    ax.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:.4f}'))

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_major_locator(matplotlib.dates.HourLocator(byhour=[0, 6, 12, 18]))
    ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_minor_locator(matplotlib.dates.HourLocator(byhour=range(0, 24, 1)))
    ax.xaxis.set_tick_params(rotation=90)
    ax.xaxis.set_tick_params(rotation=90, which="minor")
    ax.set_ylim(bottom=0)
    ax.set_ylabel(f"Occupancy Density")
    ax.set_xlabel(f"Time of Day (UTC)")
    ax.legend()

    ax.set_xlim(datetime.datetime(2021, 1, 1, 0, 0, 0),datetime.datetime(2021, 1, 2, 0, 0, 0)) # noqa
    title_str = f"Clustered Occupation Curves, by {idType.value} ({len(plotted_ids)} {idType.value}s)\n" \
                f"raster resolution: {frequency_min}min, correlation threshold: {corr_threshold}, " \
                f"merge type: {mergeType.value}, data until {datetime.datetime.now().date()}"
    if title:
        fig.suptitle(title_str, fontsize=18)

    fig.tight_layout()
    if savefig:
        fig.subplots_adjust(top=0.94)
        fp = basepath / "clusters_ac_all.png"
        plt.savefig(fp, dpi=300)
        log.info(f"[Figure {fig_num}] File was written to: {fp}")
    else:
        plt.show()



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", help="path to folder containing 'dataset.csv' (default: $PROJECT_ROOT/data)", type=str,
                        default=Path(os.path.abspath(__file__)).parent.parent / "data") # file is one folders in; so call .parent twice
    parser.add_argument("--outpath", help="path to image output (default: temporary directory)", type=str, default=None)
    args = parser.parse_args()

    if outpath := args.outpath:
        basepath_output = Path(outpath)
    else:
        basepath_output = Path(tempfile.gettempdir()) / "images"
        log.info(f"Outpath not provided, will use a temporary folder at: {basepath_output}")
    if not basepath_output.is_dir():
        log.info(f"Folder at: {basepath_output} does not exist, will try to create!")
        os.mkdir(basepath_output)

    _cacher = cacher_class.Cacher(maxCacheAge=datetime.timedelta(days=7))
    _idType = IDType.EVSE
    _mergeType = MergeType.CHARGINGS
    _frequency_min = 2
    _corr_threshold = 0.8
    _curves_per_group = 4
    _filterTrx = 10

    _start_dt = datetime.datetime(2021, 5, 6)
    _end_dt = datetime.datetime(2021, 10, 6)

    _title = False
    _savefig = True
    _figsize = (19,7)

    plt.rc('font', size=16)  # controls default text size
    plt.rc('axes', titlesize=16)  # fontsize of the title
    plt.rc('axes', labelsize=16)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=16)  # fontsize of the x tick labels
    plt.rc('ytick', labelsize=16)  # fontsize of the y tick labels
    plt.rc('legend', fontsize=16)  # fontsize of the legend

    df_paper = pd.read_csv(Path(args.dataset) / "dataset.csv", sep=";", compression="gzip",
                           parse_dates=["state_start_ts", "state_end_ts"])
    df_paper["interval_length"] = pd.to_timedelta(df_paper.interval_length)

    ## Daytime Plotting
    occupation_evse = generate_occupation(df=df_paper,
                                     cacher=_cacher,
                                     frequency_min=_frequency_min,
                                     dt_start=_start_dt,
                                     dt_end=_end_dt,
                                     idType=IDType.EVSE,
                                     norm=NormType.RELATIVE,
                                     filterTrx=_filterTrx)

    data, cluster_df = cluster_ac_dc(df=df_paper,
                                  occupation=occupation_evse,
                                  corr_threshold=_corr_threshold,
                                  mergeType=_mergeType,
                                  idType=IDType.EVSE,
                                  cacher=_cacher)
    cluster_ids = cluster_df.cluster_id.value_counts().reset_index().reset_index()
    cluster_df.cluster_id = cluster_df.cluster_id.replace(cluster_ids["index"].values, cluster_ids.level_0.values)
    cluster_df.sort_values(["power_type", "cluster_id"], inplace=True)

    cluster_colors = {
        0: "tab:blue",
        1: "tab:orange",
        2: "tab:green",
        3: "tab:red"
    }

    # Figure A4
    plot_clusters_ac(data[0], frequency_min=_frequency_min, corr_threshold=_corr_threshold,
                     occupation=occupation_evse, idType=_idType,
                     mergeType=_mergeType,
                     savefig=_savefig, figsize=_figsize, title=_title,
                     curves_per_group=_curves_per_group, basepath=basepath_output, cluster_colors=cluster_colors,
                     fig_num="A4")

    # Figure 9
    plot_clusters_ac_all(data[0], frequency_min=_frequency_min, corr_threshold=_corr_threshold,
                         idType=_idType,
                         mergeType=_mergeType,
                         savefig=_savefig, figsize=_figsize, title=_title,
                         curves_per_group=_curves_per_group, basepath=basepath_output, cluster_colors=cluster_colors,
                         fig_num=9)
