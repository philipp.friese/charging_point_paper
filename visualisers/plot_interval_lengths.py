import tempfile

import matplotlib.ticker
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.dates
import pandas as pd
import datetime,sys
import numpy as np
from typing import Union
from pathlib import Path

import argparse

import pathlib, os
basepath_output = pathlib.Path(os.path.dirname(os.path.realpath(__file__))).parent

pd.options.display.width = 1920
pd.options.display.max_rows = 99
pd.options.display.max_columns = 99
pd.options.display.min_rows = 99

import logging
log = logging.getLogger(__name__)
__format = "{filename}:{lineno} {asctime} {levelname[0]} - {message} "
__style = '{'
__level = logging.INFO
logging.basicConfig(format=__format, style=__style, stream=sys.stdout, level=__level) # noqa

def darken(color: tuple[float,float,float], factor: float) -> str:
    color_hsv = mcolors.rgb_to_hsv(color)
    h,s,v = color_hsv

    l = v*(1-(s/2))
    s_l = 0 if l == 0 or l==1 else (v-l)/min(l,1-l)

    l_new = l*factor

    h=h
    v=l_new+s_l*min(l_new,1-l_new)
    s = 0 if v==0 else 2*(1-(l_new/v))
    r,g,b = mcolors.hsv_to_rgb([h,s,v])
    return mcolors.rgb2hex([r,g,b])

def plot_pandas_by_powertype(df, mins, hours, basepath: Path,
                             cumulative: bool = False, title: bool = True, savefig: bool = False,
                             xlim: Union[None, tuple[float,float]] = None, fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising Interval Lengths by PowerType")
    num_xs = int(hours*60//mins)

    x = list(range(num_xs-1))
    bins = [datetime.timedelta(minutes=_x * mins) for _x in range(num_xs)]

    empty_df = pd.DataFrame(bins[:-1], columns=["interval_length"])
    empty_df["count"] = None
    empty_df.interval_length = empty_df["interval_length"].dt.total_seconds()
    empty_df.set_index("interval_length", inplace=True)
    ys = {}
    for powertype, group in df.groupby("power_type"):
        ys[powertype] = []
        _count =  pd.cut(group.interval_length, bins,labels=bins[:-1]).value_counts().reset_index()
        _count["index"] = _count["index"].dt.total_seconds()
        _count.set_index("index",inplace=True)
        _count.rename(columns={"interval_length":"count"}, inplace=True)
        ys[powertype] = empty_df.combine_first(_count).fillna(0)

    colors = [
        'tab:blue',
        'tab:red',
        'tab:green',
        'tab:orange',
        'tab:purple',
        'tab:brown',
        'tab:pink',
        'tab:gray',
        'tab:olive',
        'tab:cyan'
    ]

    num_filter = int(np.floor((48*60//mins)/96))

    xticks = list(range(num_xs-1))
    xlabels = [datetime.timedelta(minutes=mins*x).__str__() for x in xticks]

    for idx, powertype in enumerate(ys.keys()):
        fig, ax = plt.subplots(figsize=(19, 7))
        fig: plt.Figure
        ax: plt.Axes
        lines, labels = [], []
        y = np.array(ys[powertype]["count"].values)
        ax.bar(x, height=y, label=f"{powertype} histogram", color=colors[idx])

        li,la = ax.get_legend_handles_labels()
        lines += li
        labels += la
        if cumulative:
            twin_ax = ax.twinx()
            y_cum = np.cumsum(y)
            y_cum = y_cum / max(y_cum)

            color = darken(mcolors.to_rgb(colors[idx]),0.8)
            twin_ax.plot(x, y_cum, linewidth=1 if mins < 1 else 2.5, label=f"{powertype} cumulative", color=color)

            twin_ax.set_ylabel(f"Cumulative number of charge events")
            twin_ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(1.0, decimals=0))
            twin_ax.yaxis.set_major_locator(matplotlib.ticker.FixedLocator([0.1*x for x in range(10+1)]))
            twin_ax.set_ylim(0, 1.02)
            li, la = twin_ax.get_legend_handles_labels()
            lines += li
            labels += la

        # axes and labels
        for i in [6,12,18,24,36,48]:
            ax.axvline(x=(60 * i) / mins, color="black", linewidth=1.5,zorder=0)

        ax.set_xticks(xticks[::num_filter])
        ax.set_xticklabels(xlabels[::num_filter], rotation=90)
        ax.set_xlim(left=-1, right=(60 * hours) / (mins))

        if not isinstance(xlim, type(None)):
            ax.set_xlim(left=(xlim[0]*60)/mins, right=(xlim[1]*60)/mins)
        ax.legend(lines, labels, loc="lower right")
        ax.set_ylabel("Number of Charge Events per Power Type")
        ax.set_xlabel("Charge Event Duration")
        ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

        if title:
            plt.suptitle(f"Number of charging interval durations per power type, bucket size: {mins} minutes")
        plt.tight_layout()

        if savefig:
            fp = basepath / (f"intervals_{mins}_{powertype}{'-'+str(xlim[0])+'-'+str(xlim[1]) if xlim else ''}".replace(".","_") + ".pdf")
            plt.savefig(fp, dpi=300)
            log.info(f"[Figure {fig_num}] File was written to: {fp}")
        else:
            plt.show()

def plot_pandas_by_powertype_closeup(df, mins, hours, basepath: Path,
                             title: bool = True, savefig: bool = False, powertype: str = "AC",
                             xlim: tuple[float,float] = None, fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising close-up of Interval Lengths by PowerType")

    num_xs = int(hours*60//mins)

    x = list(range(num_xs-1))
    bins = [datetime.timedelta(minutes=_x * mins) for _x in range(num_xs)]

    empty_df = pd.DataFrame(bins[:-1], columns=["interval_length"])
    empty_df["count"] = None
    empty_df.interval_length = empty_df["interval_length"].dt.total_seconds()
    empty_df.set_index("interval_length", inplace=True)
    ys = {}
    ys[powertype] = []
    group = df[df.power_type == powertype]
    _count =  pd.cut(group.interval_length, bins,labels=bins[:-1]).value_counts().reset_index()
    _count["index"] = _count["index"].dt.total_seconds()
    _count.set_index("index",inplace=True)
    _count.rename(columns={"interval_length":"count"}, inplace=True)
    ys[powertype] = empty_df.combine_first(_count).fillna(0)



    colors = [
        'tab:blue',
        'tab:red',
        'tab:green',
        'tab:orange',
        'tab:purple',
        'tab:brown',
        'tab:pink',
        'tab:gray',
        'tab:olive',
        'tab:cyan'
    ]

    #num_filter = int(np.floor((48*60//mins)/96))

    xticks = list(range(num_xs-1))
    xlabels = [datetime.timedelta(minutes=mins*x).__str__() for x in xticks]

    idx = 0 if powertype == 'AC' else 1

    fig, ax = plt.subplots(figsize=(19, 7))
    fig: plt.Figure
    ax: plt.Axes
    lines, labels = [], []
    y = np.array(ys[powertype]["count"].values)
    ax.bar(x, height=y, label=f"{powertype} histogram", color=colors[idx])

    li,la = ax.get_legend_handles_labels()
    lines += li
    labels += la


    # axes and labels
    for i in [6,12,18,24,36,48]:
        ax.axvline(x=(60 * i) / mins, color="black", linewidth=1.5,zorder=0)


    ax.set_xticks(xticks[::4])
    ax.set_xticklabels(xlabels[::4], rotation=90)
    ax.set_ylim(bottom=0,top=max(y[int((xlim[0]*60)/mins):int((xlim[1]*60)/mins)]))
    ax.set_xlim(left=-1, right=(60 * hours) / (mins))
    # ax.set_ylim(top=max_y)
    if not isinstance(xlim, type(None)):
        ax.set_xlim(left=(xlim[0]*60)/mins, right=(xlim[1]*60)/mins)
    #ax.legend(lines, labels, loc="lower right")
    ax.set_ylabel("Number of Charge Events")
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

    plt.xlabel("Charge Event Length")


    if title:
        plt.suptitle(f"Number of charging interval durations per power type, bucket size: {mins} minutes")
    plt.tight_layout()

    if savefig:
        fp = basepath / (f"intervals_closeup_{mins}_{powertype}".replace(".","_") + ".pdf")
        plt.savefig(fp, dpi=300)
        log.info(f"[Figure {fig_num}] File was written to: {fp}")
    else:
        plt.show()

def plot_interval_lengths_night_ac(df: pd.DataFrame, title: bool, savefig: bool, basepath: Path, figsize:tuple[float, float]=(19, 7), fig_num: Union[str,int] = "X"):
    log.info(f"[Figure {fig_num}] Visualising AC Interval Lengths during night")
    df_AC = df[df.power_type == "AC"][["interval_id","interval_length","state_start_ts","state_end_ts"]]
    df_AC["start_hour"] = df_AC.state_start_ts.dt.hour

    df_short = df_AC[(df_AC.interval_length < datetime.timedelta(hours=6))]
    df_short = df_short.groupby("start_hour").interval_id.count()
    df_short["cnt_perc"] = df_short / df_short.sum()

    df_long = df_AC[ (df_AC.interval_length >= datetime.timedelta(hours=6))
                   & (df_AC.interval_length < datetime.timedelta(hours=18))]
    df_long = df_long.groupby("start_hour").interval_id.count()
    df_long["cnt_perc"] = df_long / df_long.sum()


    fig,ax = plt.subplots(figsize=figsize)
    xticks = np.array(list(range(24)))
    xlabels = [datetime.timedelta(hours=int(x)).__str__()[:-3] for x in xticks]

    ax.bar(xticks+0.175,df_short.cnt_perc.values,width=0.3,label=f"0h to 6h",color="tab:blue")
    ax.bar(xticks-0.175,df_long.cnt_perc.values,width=0.3,label=f"6h to 18h",color="tab:orange")

    ax.set_xlabel("Time of Day (UTC) of charge event start")
    ax.set_ylabel(f"Number of charge events (%, relative to group)")
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(1.0, decimals=0))
    ax.set_xticks(xticks)
    ax.set_xticklabels(xlabels, rotation=90)
    ax.set_xlim(left=-1,right=24)
    ax.legend(title="Charge Event Length")

    if title: plt.suptitle(f"")
    plt.tight_layout()

    if savefig:
        fp = basepath / "intervals_comparison.pdf"
        plt.savefig(fp, dpi=300)
        log.info(f"[Figure {fig_num}] File was written to: {fp}")
    else:
        plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", help="path to folder containing 'dataset.csv' (default: $PROJECT_ROOT/data)", type=str,
                        default=Path(os.path.abspath(__file__)).parent.parent / "data") # file is one folders in; so call .parent twice
    parser.add_argument("--outpath", help="path to image output (default: temporary directory)", type=str, default=None)
    args = parser.parse_args()

    if (outpath := args.outpath):
        basepath_output = Path(outpath)
    else:
        basepath_output = Path(tempfile.gettempdir()) / "images"
        log.info(f"Outpath not provided, will use a temporary folder at: {basepath_output}")
    if not basepath_output.is_dir():
        log.info(f"Folder at: {basepath_output} does not exist, will try to create!")
        os.mkdir(basepath_output)

    _hours = 24
    _start_dt = datetime.datetime(2021,5,6)
    _end_dt = datetime.datetime(2021,10,6)

    plt.rc('font', size=16)  # controls default text size
    plt.rc('axes', titlesize=16)  # fontsize of the title
    plt.rc('axes', labelsize=16)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=16)  # fontsize of the x tick labels
    plt.rc('ytick', labelsize=16)  # fontsize of the y tick labels
    plt.rc('legend', fontsize=16)  # fontsize of the legend

    _title = False
    _savefig = True

    # from connectors.postgresConnector import PSQL
    # psql = PSQL()
    # cursor = psql.cursor()
    # query = """
    #         select interval_id, evse_id, operators.operator_id, state, state_start_ts, state_end_ts, state_end_ts - state_start_ts,
    #             lat,lon, power_rating_kw, plug_type, power_type
    #         from connector_state_intervals_source
    #         join connectors c on connector_state_intervals_source.connector_id = c.connector_id
    #         join operators on substring(evse_id, 3,3) = operators.operator_id
    #         join plug_types pt on c.plug_type_id = pt.plug_type_id
    #         join power_types p on c.power_type_id = p.power_type_id
    #         join regions r on st_contains(r.geom, c.geom)
    #         where  source_id = 2
    #            and state = 2
    #            and state_start_ts >= %s
    #            and state_start_ts < %s
    #            and state_end_ts is not null
    #            and state_start_ts is not null
    #            and (state_end_ts - state_start_ts) <= interval '24h'
    #            and power_type = any(array['AC','DC'])"""
    # cursor.execute(query, vars=[_start_dt, _end_dt])
    # data = cursor.fetchall()
    # df = pd.DataFrame(data, columns=[
    #     "interval_id",
    #     "evse_id",
    #     "operator_id",
    #     "state",
    #     "state_start_ts",
    #     "state_end_ts",
    #     "interval_length",
    #     "lat", "lon",
    #     "power_rating_kw",
    #     "plug_type", "power_type"
    # ]).sort_values("interval_length")
    #
    # df.to_csv("/tmp/paper_data.csv",sep=";",index=False, compression="gzip")
    # exit()
    df = pd.read_csv(Path(args.dataset) / "dataset.csv",sep=";",compression="gzip",
                     parse_dates=["state_start_ts","state_end_ts"])
    df["interval_length"] = pd.to_timedelta(df.interval_length)

    # Figure 3
    plot_pandas_by_powertype(df, 10, _hours, basepath=basepath_output, cumulative=True, title=_title, savefig=_savefig, fig_num=3)

    # Figure 4
    plot_interval_lengths_night_ac(df=df, title=_title, basepath=basepath_output, savefig=_savefig, fig_num=4)

    # Figure 5
    plot_pandas_by_powertype(df, 0.5, _hours, basepath=basepath_output, cumulative=False, title=_title, savefig=_savefig,
                             xlim=(0,6), fig_num=5)
    # Figure 6
    plot_pandas_by_powertype_closeup(df, 0.5, _hours, basepath=basepath_output, powertype="AC", title=_title, savefig=_savefig,
                                     xlim=(1.25,2.25), fig_num=6)

    # Figure A1
    plot_pandas_by_powertype(df, 0.5, _hours, basepath=basepath_output, cumulative=True, title=_title, savefig=_savefig, fig_num="A1")






