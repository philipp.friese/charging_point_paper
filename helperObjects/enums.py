import enum

class NormType(enum.Enum):
    NONE = 0
    MAX_POSSIBLE = 1
    RELATIVE = 2
    L2 = 3
    RESCALE = 4

class MergeType(enum.Enum):
    IDS = "ids"
    CHARGINGS = "chargings"

class IDType(enum.Enum):
    NONE=None
    EVSE = "evse_id"
    CHARGING_PARK = "charging_park_id"
    REGION = "region_id"