import numpy as np
import pandas as pd

from helperObjects.enums import MergeType

class MergeRecord:
    def __init__(self, ids: set[str], trx: int):
        """
        A MergeRecord stores the number of contributing IDs and transactions / chargings to a merged occupation curve

        :param ids: set of contributing IDs
        :param trx: number of total transactions/ chargings across all IDs
        """
        self.ids = ids
        self.trx = trx

    def __add__(self, other):
        assert len(self.ids & other.ids) == 0, "IDs of given merge record overlaps! One ID cannot contribute to two occupation curves simultaneously"
        return MergeRecord(self.ids | other.ids, # set union of ids
                           self.trx + other.trx) # addition of number of transactions


class ClusterResult:
    def __init__(self,
                 curves: np.ndarray,
                 merge_records: list[MergeRecord],
                 correlations: list[float],
                 cluster_type: str = ''):
        """
        A ClusterResult bundles the result of an agglomerative clustering run.

        :param curves: contains the clustered & averaged curves per cluster
        :param merge_records: contains a list of MergeRecords per cluster; length: number of clusters
        :param correlations: contains the maximum observed similarity (correlation) per iteration; length: number of iterations
        :param cluster_type: optional type string
        """
        self.curves = curves
        self.merge_records = merge_records
        self.correlations = correlations
        self.cluster_type = cluster_type

    def sort(self, mergeType: MergeType):
        zipped = zip(self.curves, self.merge_records)
        if mergeType is MergeType.IDS:
            zipped = sorted(zipped, key=lambda x: len(x[1].ids), reverse=True)
        elif mergeType is MergeType.CHARGINGS:
            zipped = sorted(zipped, key=lambda x: x[1].trx, reverse=True)
        else:
            raise NotImplementedError(f"Merge Type {mergeType.value} is not implemented!")
        self.curves = [t[0] for t in zipped]
        self.merge_records = [t[1] for t in zipped]

    def filter_top_n(self, n: int, mergeType: MergeType) -> tuple[list[float], list[MergeRecord]]:
        self.sort(mergeType=mergeType)
        return self.curves[:n], self.merge_records[:n]


    def calculate_cluster_stdvariance(self, occupation_T: pd.DataFrame):
        variances = []
        for idx, cluster_record in enumerate(self.merge_records):
            if len(cluster_record.ids) == 1:
                continue
            curves = occupation_T.loc[(cluster_record.ids)].values
            variances.append(np.std(curves, 0))
        return np.array(variances)