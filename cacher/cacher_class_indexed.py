import pandas as pd
import logging
import tempfile
import os
import datetime
import lzma
import pickle
import enum
import hashlib

class CacheType(enum.Enum):
	COMPRESSED = 1
	NORMAL = 2

def hashDf(df: pd.DataFrame, dependent_cols: list = None) -> str:
	# search for 'O'/object dtype columns
	# if found: remove them from the hashed df-subset, __unless__ they are a string
	# reasoning: object-cols with e.g. list- or set-values cannot be hashed by pd.util.hash_pandas_object
	dtypes = [(idx, val) for idx, val in enumerate(df.dtypes)]
	dtypes = list(filter(lambda tup: tup[1].kind == 'O', dtypes))
	object_cols = set(df.columns[tup[0]] for tup in dtypes)
	final_cols = set(df.columns)
	for col in object_cols:
		if not isinstance(df[col].iloc[0], str):
			final_cols.remove(col)

	# if depenent_cols was given, use that instead of all cols minus those previously determined object-cols
	# reasoning: using a subset for determining the hash allows for the overall df to slightly change (e.g. in unrelated columns)
	#  without altering the hash -> cache-usage possible eventhough the df changed
	if not isinstance(dependent_cols, type(None)):
		final_cols &= set(dependent_cols)

	return str(pd.util.hash_pandas_object(df[sorted(list(final_cols))]).sum())


def hasher(items, dependent_df_cols: dict = None) -> str:
	hash_str = ""
	for idx, (key, element) in enumerate(items):
		if isinstance(element, pd.DataFrame):
			hash_str += hashDf(element, dependent_df_cols[key] if dependent_df_cols and key in dependent_df_cols else None)
		else:
			hash_str += str(element)
	return hashlib.sha1(hash_str.encode("utf8")).hexdigest()


class Cacher:
	def __init__(self, cachePath=None, cacheFolder: str = "caches", shouldCache=True, shouldCompress=True,
				 maxCacheAge: datetime.timedelta = datetime.timedelta(days=1),
				 createCacheFolder=True, deleteOutdatedCacheFile=True, deleteThrowingCacheFile=True):
		self.log = logging.getLogger(__name__)

		if isinstance(cachePath, type(None)):
			newCachePath = os.path.join(tempfile.gettempdir(), cacheFolder)
			self.log.debug(f"No cache path given, will use {newCachePath}")
			self.cachePath = newCachePath
		else:
			self.cachePath = cachePath

		if createCacheFolder and not os.path.isdir(self.cachePath):
			self.createCacheFolder = createCacheFolder
			self.log.debug(f"The cache folder at {self.cachePath} does not exist! Will create..")
			os.mkdir(self.cachePath)

		self.shouldCompress = shouldCompress
		self.shouldCache = shouldCache


		self.deleteOutdatedCacheFile = deleteOutdatedCacheFile
		self.deleteThrowingCacheFile = deleteThrowingCacheFile
		self.maxCacheAge = maxCacheAge

		self.fileEndingCompressed = ".pkl.xz"
		self.fileEndingNormal = ".pkl"

		self._initCaches()

	def _initCaches(self):
		self.caches = {}
		now = datetime.datetime.now()
		for entry in os.scandir(self.cachePath):
			entry: os.DirEntry
			file_name = entry.name
			file_timestamp = datetime.datetime.fromtimestamp(int(entry.stat().st_ctime))
			file_age = now - file_timestamp
			if entry.is_file():
				if file_name.endswith(self.fileEndingCompressed):
					file_cache_id = file_name[:-(len(self.fileEndingCompressed))]
					cache_type = CacheType.COMPRESSED
				elif file_name.endswith(self.fileEndingNormal):
					file_cache_id = file_name[:-(len(self.fileEndingNormal))]
					cache_type = CacheType.NORMAL
				else:
					self.log.debug(f"Unknown file ending on {entry.name}, will skip!")
					continue

				if file_cache_id not in self.caches or (
						file_cache_id in self.caches and self.caches[file_cache_id]["timestamp"] < file_timestamp):
					self._updateCache(file_cache_id, file_name, file_timestamp, cache_type)
				elif file_age.total_seconds() <= self.maxCacheAge.total_seconds() \
						and self.deleteOutdatedCacheFile:
					self.log.debug(f"Found outdated cache {entry}, will delete!")
					os.remove(entry)

	def _updateCache(self, cacheId: str, file_name: str, timestamp: datetime.datetime, cache_type: CacheType):
		self.caches[cacheId] = {"file": file_name, "timestamp": timestamp, "cache_type": cache_type}

	def _getCache(self, cacheId: str):
		if cacheId not in self.caches:
			return None
		entry = self.caches[cacheId]
		current_datetime = datetime.datetime.now()
		cache_timestamp = entry["timestamp"]
		cacheAge = current_datetime - cache_timestamp
		if ((self.maxCacheAge.total_seconds() > 0 and cacheAge.total_seconds() <= self.maxCacheAge.total_seconds()) or
				self.maxCacheAge.total_seconds() == 0):
			self.log.debug(f"Found cache for cacheId {cacheId}: {self.caches[cacheId]}")
			return self.caches[cacheId]
		else:
			if self.maxCacheAge.total_seconds() > 0:
				self.log.debug(f"Found outdated cache ({cache_timestamp})")
			if self.deleteOutdatedCacheFile:
				self.log.info(f"Will delete outdated cache ({cache_timestamp})")
				os.remove(os.path.join(self.cachePath, entry["file"]))
			return None

	def cacheExists(self, cacheId) -> bool:
		return not isinstance(self._getCache(cacheId=cacheId), type(None))

	def attemptCache(self, cacheId: str, callback, kwargs: dict, generate_hash: bool = False, ckwargs: dict = {}):
		"""
		Try to load a cache file on the specified cachePath with the specified cacheId, saved in the format: <cachePath>/<cacheId>-<timestamp>.<ext>
		If it is successful, return the cachefile. Otherwise, call the specified callback, cache the result for later reuse and return the data.

		Note: ckwargs is currently only used for passing dependent_cols to hasher. This might change in the future

		:param cacheId: cache id for saving cache files (needs to be unique for each cache)
		:param callback: function to call if cache file does not exists
		:param kwargs: dictionary containing all necessary callback function parameters
		:param generate_hash: whether or not to generate hash based on given kwargs.values()
		:param ckwargs: keyworded args for internal cacher-functionality, see notes
		:return: result from callback or cachefile
		"""

		if not self.shouldCache:
			self.log.debug(f"shouldCache is set to false, will call callback directly")
			return callback(**kwargs)

		if not os.path.isdir(self.cachePath):
			logging.warning(f"Cache path {self.cachePath} is not a directory!")
			if self.createCacheFolder:
				self.log.debug(f"Creating cache folder at {self.cachePath}")
				os.mkdir(self.cachePath)

		if generate_hash:
			cacheId += "-" + hasher(kwargs.items(), **ckwargs)

		cache_entry = self.directCache(cacheId)

		if cache_entry is not None:
			return cache_entry

		self.log.debug(f"Did not find valid cache for cacheId {cacheId}, using fallback..")
		try:
			data = callback(**kwargs)
		except TypeError as te:
			logging.error(f"Error while calling callback {callback} with arguments {kwargs}: {te}")
			raise

		self.writeCache(cacheId, data)
		data = self.directCache(cacheId)  # read data from cache
		return data

	def writeCache(self, cacheId: str, data):
		"""
		Write data directly using the given cacheId
		:param cacheId: cache id for saving cache files (needs to be unique for each cache)
		:param data: data to write to cache file
		:return:
		"""

		if self.shouldCompress:
			file_name = os.path.join(self.cachePath, f"{cacheId}{self.fileEndingCompressed}")
			cache_type = CacheType.COMPRESSED
			outfile = lzma.open(file_name, "w")
		else:
			file_name = os.path.join(self.cachePath, f"{cacheId}{self.fileEndingNormal}")
			cache_type = CacheType.NORMAL
			outfile = open(file_name, "w")
		pickle.dump(data, outfile)
		outfile.close()

		self.log.debug(f"Written data with id {cacheId} to location {file_name}")

		self._updateCache(cacheId, file_name, datetime.datetime.now(), cache_type)

	def directCache(self, cacheId: str):
		"""
		Attempt to read a cache using cacheId, returns None if attempt fails
		:param cacheId: cache id for reading cache file
		:return: cached data if successful, else None
		"""

		cache_entry = self._getCache(cacheId)
		if cache_entry:
			file_name = cache_entry["file"]
			filepath = os.path.join(self.cachePath, file_name)
			cache_type: CacheType = cache_entry["cache_type"]
			if cache_type is CacheType.COMPRESSED:
				cacheInfile = lzma.open(filepath, "r")
			elif cache_type is CacheType.NORMAL:
				cacheInfile = open(filepath, "r")
			else:
				self.log.error(f"Unsupported CacheType: {cache_type}")
				return None
			try:
				cache = pickle.load(cacheInfile)
				cacheInfile.close()
			except EOFError as e:
				self.log.error(f"Error while unpickling file {filepath}: {e}")
				if self.deleteThrowingCacheFile:
					self.log.warning(f"Will delete throwing cache")
					os.remove(filepath)
				return None
			return cache

		self.log.debug(f"Did not find cache for {cacheId}")
		return None

	def preflush(self, cacheId, data):
		with lzma.open(os.path.join(self.cachePath,
									f"preflush--{cacheId}--{int(datetime.datetime.now().timestamp())}{self.fileEndingCompressed}"),
					   "w") as outfile:
			pickle.dump(data, outfile)
