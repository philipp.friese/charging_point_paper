import datetime
import multiprocessing
import sys
import logging

import numpy as np
import pandas as pd
from sklearn import preprocessing, ensemble, metrics
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline

log = logging.getLogger(__name__)
__format = "{filename}:{lineno} {asctime} {levelname[0]} - {message} "
__style = '{'
__level = logging.INFO
logging.basicConfig(format=__format, style=__style, stream=sys.stdout, level=__level) # noqa


def _calc_elapsed(start_time: datetime.datetime, current_count: int, total_count: int) -> tuple[datetime.timedelta, datetime.datetime]:
    perf_current_time = datetime.datetime.now()
    elapsed_time = perf_current_time - start_time
    expected_total_time = (elapsed_time / current_count) * total_count
    expected_remaining_time = expected_total_time - elapsed_time
    if expected_remaining_time < datetime.timedelta(seconds=0):  # avoid negative ETA
        expected_remaining_time = datetime.timedelta(seconds=0)
    expected_finish = perf_current_time + expected_remaining_time
    return expected_remaining_time, expected_finish


class Optimiser(multiprocessing.Process):
    def __init__(self, queue, chunk, dtypes, cols, X, y,
                 status_array, proc_idx):
        super(Optimiser, self).__init__()

        self.queue = queue
        self.X = X
        self.y = y
        self.cols = cols
        self.dtypes = dtypes
        self.chunk = chunk

        self.status_array = status_array
        self.proc_idx = proc_idx

        self._pconn, self._cconn = multiprocessing.Pipe()  # noqa
        self._exception = None

    def run(self):
        X_train, X_test, y_train, y_test = train_test_split(self.X, self.y, random_state=1, test_size=0.25)

        for idx, tup in enumerate(self.chunk):
            par_set = {k: self.dtypes[k](v) for k, v in zip(self.dtypes.keys(), tup)}
            regr = make_pipeline(preprocessing.StandardScaler(),
                                 ensemble.RandomForestClassifier(random_state=1, **par_set))
            regr.fit(X_train, y_train)
            scores = [regr.score(X_train, y_train),
                      regr.score(X_test, y_test),
                      regr.score(self.X, self.y),
                      metrics.accuracy_score(y_test, regr.predict(X_test)),
                      metrics.precision_score(y_test, regr.predict(X_test), average='weighted', zero_division=0),
                      metrics.recall_score(y_test, regr.predict(X_test), average='weighted', zero_division=0),
                      metrics.accuracy_score(y_train, regr.predict(X_train)),
                      metrics.precision_score(y_train, regr.predict(X_train), average='weighted', zero_division=0),
                      metrics.recall_score(y_train, regr.predict(X_train), average='weighted', zero_division=0)]

            df = pd.DataFrame([list(tup) + scores], columns=self.cols)
            self.queue.put(df)
            self.status_array[self.proc_idx] += 1

    @property
    def exception(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception



def tune(X,y):

    parameters = {
        "n_estimators": {"vals": [50, 75,100, 150,200], "dtype": int},
        "max_depth": {"vals": np.arange(5, 20 + 1, 2).tolist(), "dtype": int},
        "min_samples_split": {"vals": np.arange(3, 20 + 1, 2).tolist(), "dtype": int},
        "max_samples": {"vals": np.arange(.5, .9 + .1, 0.1).tolist(), "dtype": float},
        #"learning_rate": {"vals": [0.05, 0.1, 0.2], "dtype": float},
        "max_features": {"vals": ["sqrt", "auto", "log2"], "dtype": str},
        "class_weight": {"vals": ["balanced","balanced_subsample"], "dtype": str},
    }

    import itertools
    import multiprocessing
    import time
    import ctypes
    import random

    vals = list(itertools.product(*[parameters[k]["vals"] for k in parameters]))
    random.shuffle(vals)

    dtypes = {k: parameters[k]["dtype"] for k in parameters}

    cols = list(parameters.keys()) + ["train_score", "test_score", "total_score",
                                      "accuracy_test", "precision_test", "recall_test",
                                      "accuracy_train", "precision_train", "recall_train"]

    manager = multiprocessing.Manager()
    queue = manager.Queue(len(vals))
    num_procs = 4
    status_array = multiprocessing.Array(typecode_or_type=ctypes.c_int, lock=False,
                                         size_or_initializer=[0 for _ in range(num_procs)])
    chunks = np.array_split(vals, num_procs)

    processes = [Optimiser(queue=queue, chunk=chunk, dtypes=dtypes,
                           X=X, y=y,
                           status_array=status_array, proc_idx=idx, cols=cols)
                 for idx, chunk in enumerate(chunks)]
    start_dt = datetime.datetime.now()
    for p in processes:
        p.start()

    df = None

    while any([p.is_alive() for p in processes]):
        time.sleep(2)
        c = sum(status_array)
        if c == 0:  # avoid divide by 0
            continue
        while not queue.empty():
            df = pd.concat([df, queue.get()])

        if not isinstance(df, type(None)):
            test_max_idx = np.argmax(df.test_score.values)
            test_max = df.iloc[test_max_idx]['test_score']
            par_set = ', '.join([f"{k}:{round(float(v), 2) if str(v).isdigit() else v}" for k, v in
                                 zip(parameters.keys(), df.iloc[test_max_idx][list(parameters.keys())].values)])
            remaining, finish = _calc_elapsed(start_dt, c + 1, len(vals))
            print((f"\r[{c:>{len(str(len(vals)))}}/{len(vals)}] Remaining Time: {remaining}, finishing at {finish} | "
                   f"Test Max: {test_max:.4f} with {par_set}").ljust(300), end="")
    print()

    for p in processes:
        p.join()
        if tupl := p.exception:
            print(tupl[0])
            print(tupl[1])

    while not queue.empty():
        df = pd.concat([df, queue.get()])

    df.to_csv("hyperparameter_optimisation.csv", sep=";", index=False)
