import logging
import pandas as pd

from helperObjects.enums import MergeType, IDType
from helperObjects.classes import ClusterResult
from algorithms.correlation_clustering import cluster

log = logging.getLogger(__file__)

pd.options.display.max_columns=99
pd.options.display.width=1920
pd.options.display.max_rows=48
pd.options.display.min_rows=48

def cluster_ac_dc(df: pd.DataFrame, occupation: pd.DataFrame, cacher, idType: IDType,
                  corr_threshold: float, mergeType: MergeType) -> tuple[list[ClusterResult],pd.DataFrame]:
    assert idType == IDType.EVSE

    data = []
    cluster_data = []

    chargings_df = df.groupby("evse_id").interval_id.count().reset_index().rename(columns={"evse_id":"id",
                                                                                           "interval_id":"num_chargings"})

    for power_type, group in df.groupby("power_type"):
        evse_ids = sorted(group.evse_id.values)
        chargings_subset = chargings_df[chargings_df["id"].isin(evse_ids)].copy(deep=True)
        occupation_subset = occupation[list(set(evse_ids) & set(occupation.columns.values))].copy(deep=True)

        result = cluster(occupation_subset, corr_threshold=corr_threshold, cacher=cacher,
                         chargings_df=chargings_subset, mergeType=mergeType)
        result.cluster_type = power_type
        data.append(result)
        cluster_data += [(power_type, idx, evse_id, len(merge_record.ids))
                         for idx, merge_record in enumerate(result.merge_records)
                         for evse_id in merge_record.ids]
    cluster_df = pd.DataFrame(cluster_data, columns=["power_type", "cluster_id", "evse_id", "cluster_size"])
    return data,cluster_df

