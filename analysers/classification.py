import datetime
import pickle
import sys
import tempfile
from pathlib import Path

import argparse
import numpy as np
import os
import pandas as pd
from sklearn import ensemble
from sklearn import preprocessing, metrics
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline

import cacher.cacher_class_indexed as cacher_class
from aggregators.occupation import generate_occupation
from analysers.occupation_clustering import cluster_ac_dc
from helperObjects.classes import ClusterResult
from helperObjects.enums import MergeType, IDType, NormType

pd.options.display.max_rows=99
pd.options.display.min_rows=50
pd.options.display.max_columns=99

import matplotlib.pyplot as plt
import matplotlib.dates
import matplotlib.ticker
import logging
log = logging.getLogger(__name__)
__format = "{filename}:{lineno} {asctime} {levelname[0]} - {message} "
__style = '{'
__level = logging.INFO
logging.basicConfig(format=__format, style=__style, stream=sys.stdout, level=__level) # noqa


def plot_clusters(curves: list[tuple[int, int, list[float]]],
                  cluster_colors: dict,
                  cluster_filter: list[int] = None, cluster_mapping: dict = None,
                  frequency_min: int = 10, figsize: tuple[float, float] = (12, 12)):
    if isinstance(cluster_mapping, type(None)):
        cluster_mapping = {}
    fig, ax = plt.subplots(figsize=figsize)

    num_slices = int((60 * 24) // frequency_min)
    xticks = list(range(num_slices))
    xlabels = [(datetime.datetime(2021, 1, 1, 0, 0, 0) + datetime.timedelta(minutes=frequency_min * x))
               for x in xticks]

    for idx, vals in enumerate(curves):
        cluster_id = vals[0]
        if not cluster_filter or cluster_id in cluster_filter:
            cluster_size = vals[1]
            y = vals[2]
            new_cluster = cluster_mapping.get(cluster_id, None)
            color = cluster_colors.get(new_cluster, "grey")
            ax.plot(xlabels, y, label=f"Cluster {new_cluster}, was {cluster_id}, (size: {cluster_size})",
                    color=color)

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_major_locator(matplotlib.dates.HourLocator(byhour=[0, 6, 12, 18]))
    ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_minor_locator(matplotlib.dates.HourLocator(byhour=range(0, 24, 1)))
    ax.xaxis.set_tick_params(rotation=90)
    ax.xaxis.set_tick_params(rotation=90, which="minor")
    ax.set_xlim(datetime.datetime(2021, 1, 1, 0, 0, 0),
                datetime.datetime(2021, 1, 2, 0, 0, 0))
    plt.legend()
    plt.tight_layout()
    plt.show()


def plot_curves(_curves: list[list[float]],
                figsize: tuple[float, float] = (12, 12)):
    fig, ax = plt.subplots(figsize=figsize)
    frequency_min = 10
    num_slices = int((60 * 24) // frequency_min)
    xticks = list(range(num_slices))
    xlabels = [(datetime.datetime(2021, 1, 1, 0, 0, 0) + datetime.timedelta(minutes=frequency_min * x))
               for x in xticks]
    for idx, vals in enumerate(_curves):
        y = vals
        ax.plot(xlabels, y, label=f"Idx {idx}")

    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_major_locator(matplotlib.dates.HourLocator(byhour=[0, 6, 12, 18]))
    ax.xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ax.xaxis.set_minor_locator(matplotlib.dates.HourLocator(byhour=range(0, 24, 1)))
    ax.xaxis.set_tick_params(rotation=90)
    ax.xaxis.set_tick_params(rotation=90, which="minor")
    ax.set_xlim(datetime.datetime(2021, 1, 1, 0, 0, 0),
                datetime.datetime(2021, 1, 2, 0, 0, 0))
    # plt.legend()
    plt.tight_layout()
    plt.show()


def get_cluster_data(df_paper, cacher, frequency_min, start_dt, end_dt, corr_threshold, filter_trx):
    occupation_evse = generate_occupation(df=df_paper,
                                          cacher=cacher,
                                          frequency_min=frequency_min,
                                          dt_start=start_dt,
                                          dt_end=end_dt,
                                          idType=IDType.EVSE,
                                          norm=NormType.RELATIVE,
                                          filterTrx=filter_trx)

    data, cluster_df = cluster_ac_dc(df=df_paper,
                                     occupation=occupation_evse,
                                     corr_threshold=corr_threshold,
                                     mergeType=MergeType.CHARGINGS,
                                     idType=IDType.EVSE,
                                     cacher=cacher)
    cluster_df = cluster_df[cluster_df.power_type == 'AC']
    mapped_data = {k: v for k, v in zip(["AC", "DC"], data)}
    cluster_curves = []
    cluster_ids = []
    for idx, row in cluster_df.iterrows():
        power_type = row.power_type
        datum_subset: ClusterResult = mapped_data[power_type]
        cluster_id = row.cluster_id
        if cluster_id in cluster_ids:
            continue
        cluster_ids.append(cluster_id)
        cluster_curves.append([cluster_id,
                               len(datum_subset.merge_records[row.cluster_id].ids),
                               list(datum_subset.curves[row.cluster_id])])
    return cluster_df, cluster_curves


def get_data(cluster_df, socio_demo_df):

    socio_demo_df = socio_demo_df \
        .drop(columns=["cluster_id", "cluster_size"],errors="ignore") \
        .merge(cluster_df[["evse_id", "cluster_id", "cluster_size"]],
               left_on="evse_id", right_on="evse_id")

    # socio_demo_df = socio_demo_df[["lcgchar_priv", "lcgchar_comm", "lcgchar_sum", "hh_ek900","kk_ew",
    #          "power_type", "cluster_size", "cluster_id"]]


    df_AC = socio_demo_df[(socio_demo_df.power_type == 'AC')]
    df_cleaned = df_AC
    df_cleaned = df_cleaned[df_AC.cluster_size >= 200]

    print(f"Number of train samples: {len(df_cleaned)}/{len(df_AC)} ({round(len(df_cleaned) / len(df_AC) * 100, 1)}%)")
    print(f"Number of unique clusters: {len(df_cleaned.cluster_id.unique())} ({df_cleaned.cluster_id.unique()})")

    df_filtered = df_cleaned

    df_classify = df_filtered.drop(columns=["evse_id", "num_trx",
                                            "power_rating_kw",
                                            "plug_type", "connector_id",
                                            "power_type", "cluster_size",
                                            "intersecting_regions",
                                            ], errors="ignore")
    return df_classify

def get_prediction_overview(regr, X,y, X_test):

    vc_actual = pd.Series(y, name="num_cps_actual") \
        .value_counts() \
        .sort_index() \
        .reset_index() \
        .set_index("index")
    vc_actual.index.rename("cluster_id", inplace=True)
    vc_actual["percentage_actual"] = (vc_actual.num_cps_actual / vc_actual.num_cps_actual.sum() * 100).round(2)

    vc_predicted = pd.Series(regr.predict(X), name="num_cps_predited") \
        .value_counts() \
        .sort_index() \
        .reset_index() \
        .set_index("index")
    vc_predicted.index.rename("cluster_id", inplace=True)
    vc_predicted["percentage_predicted"] = (
                vc_predicted.num_cps_predited / vc_predicted.num_cps_predited.sum() * 100).round(2)

    vc_test_pred = pd.Series(regr.predict(X_test), name="num_cps_predicted_test") \
        .value_counts() \
        .sort_index() \
        .reset_index() \
        .set_index("index")
    vc_test_pred.index.rename("cluster_id", inplace=True)
    vc_test_pred["percentage_predicted_test"] = (
                vc_test_pred.num_cps_predicted_test / vc_test_pred.num_cps_predicted_test.sum() * 100).round(2)

    vc_df = vc_predicted.join(vc_actual).join(vc_test_pred)
    # print(vc_df)

def plot_feature_importance(regr, X_train,y_train, cols):

    from sklearn import inspection
    perm_importance = list(inspection.permutation_importance(regr, X_train, y_train).values())

    feature_df = pd.DataFrame(zip(cols, regr[1].feature_importances_,
                                  perm_importance[0], perm_importance[1]),
                              columns=["feature", "importance", "perm_importance_mean",
                                       "perm_importance_mean_importance_std"])
    feature_df.set_index("feature", inplace=True)

    fig, ax = plt.subplots(figsize=(12, 12))

    ax.bar(np.arange(len(feature_df)) - 0.2, feature_df.importance, width=0.4, label="importance")
    tax = ax.twinx()

    tax.bar(np.arange(len(feature_df)) + 0.2, feature_df.perm_importance_mean, width=0.4,
            label="permutation importance",
            color="tab:orange")

    ax.set_xticks(range(len(feature_df)))
    ax.set_xticklabels(list(feature_df.index), rotation=90)
    max_y = max(feature_df.importance.max(), feature_df.perm_importance_mean.max()) * 1.05
    min_y = min(0, min(feature_df.importance.min(), feature_df.perm_importance_mean.min()))
    ax.set_ylim(min_y, max_y)
    tax.set_ylim(min_y, max_y)

    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = tax.get_legend_handles_labels()
    ax.legend(lines + lines2, labels + labels2, loc=0)
    plt.tight_layout()
    plt.show()

def plot_confusion_matrix(regr, X, y):
    fig, ax = plt.subplots(figsize=(12, 12))
    metrics.ConfusionMatrixDisplay(metrics.confusion_matrix(y, regr.predict(X))).plot(ax=ax)
    plt.show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", help="path to folder containing 'dataset.csv' (default: $PROJECT_ROOT/data)",
                        type=str,
                        default=Path(os.path.abspath(
                            __file__)).parent.parent / "data")  # file is one folders in; so call .parent twice
    parser.add_argument("--outpath", help="path to image output (default: temporary directory)", type=str, default=None)
    args = parser.parse_args()

    if outpath := args.outpath:
        basepath_output = Path(outpath)
    else:
        basepath_output = Path(tempfile.gettempdir()) / "images"
        log.info(f"Outpath not provided, will use a temporary folder at: {basepath_output}")
    if not basepath_output.is_dir():
        log.info(f"Folder at: {basepath_output} does not exist, will try to create!")
        os.mkdir(basepath_output)

    _cacher = cacher_class.Cacher(maxCacheAge=datetime.timedelta(days=7))

    _corr_threshold = 0.8
    _frequency_min = 2
    _filter_trx = 10
    _start_dt = datetime.datetime(2021, 5, 6)
    _end_dt = datetime.datetime(2021, 10, 6)
    df_paper = pd.read_csv(Path(args.dataset) / "dataset.csv", sep=";", compression="gzip",
                           parse_dates=["state_start_ts", "state_end_ts", "interval_length"])
    df_paper["interval_length"] = pd.to_timedelta(df_paper.interval_length)

    socio_demo_df = pd.read_csv("socio_demo_dataset.csv", sep=";")

    cluster_df, cluster_curves = get_cluster_data(df_paper, cacher=_cacher, frequency_min=_frequency_min,
                                  start_dt=_start_dt, end_dt=_end_dt,
                                  corr_threshold=_corr_threshold, filter_trx=_filter_trx)
    df_classify = get_data(cluster_df=cluster_df, socio_demo_df=socio_demo_df)

    _df = df_classify.copy()
    y_raw = _df.pop('cluster_id')
    X = _df.values
    cluster_map = {
        0:0,
        1:3,
        3:1,
        4:2
    }

    cluster_colors = {
        0: "tab:blue",
        1: "tab:orange",
        2: "tab:green",
        3: "tab:red"
    }


    cluster_map_inverse = {v:k for k,v in cluster_map.items()}
    # le = preprocessing.LabelEncoder()
    # le.fit(list(np.unique(y_raw)))
    y = [cluster_map[_y] for _y in y_raw]


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1)


    regr = make_pipeline(preprocessing.StandardScaler(), ensemble.RandomForestClassifier(n_estimators=200,
                                                                                         max_depth=17,
                                                                                         min_samples_split=3,
                                                                                         max_features="auto",
                                                                                         max_samples=0.6,
                                                                                         class_weight="balanced",
                                                                                         random_state=1))
    regr.fit(X_train, y_train)


    with open(basepath_output / f"model_{datetime.datetime.now().date()}.pkl", "wb") as outfile:
        pickle.dump(regr, outfile)

    print(f"Train Score: {regr.score(X_train, y_train):.3}")
    print(f"Test  Score: {regr.score(X_test,  y_test ):.3}")
    print(f"Total Score: {regr.score(X,       y      ):.3}")

    print()
    print(f"Test score breakdown")
    print(f"Accuracy   : {metrics.accuracy_score (y_test, regr.predict(X_test)):.3}")
    print(f"Precision  : {metrics.precision_score(y_test, regr.predict(X_test),average='weighted',zero_division=0):.3}")
    print(f"Recall     : {metrics.recall_score   (y_test, regr.predict(X_test),average='weighted',zero_division=0):.3}")

    print()
    print(f"Train score breakdown")
    print(f"Accuracy   : {metrics.accuracy_score (y_train, regr.predict(X_train)):.3}")
    print(f"Precision  : {metrics.precision_score(y_train, regr.predict(X_train),average='weighted',zero_division=0):.3}")
    print(f"Recall     : {metrics.recall_score   (y_train, regr.predict(X_train),average='weighted',zero_division=0):.3}")

    # print(metrics.classification_report(y_test, regr.predict(X_test), zero_division=0))
    # print(metrics.classification_report(y_train, regr.predict(X_train), zero_division=0))
    # print(metrics.classification_report(y, regr.predict(X), zero_division=0))


    target_labels = np.unique(regr.predict(X))
    # old_clusters = list(le.inverse_transform(target_labels))
    old_clusters = list( [cluster_map_inverse[_y] for _y in target_labels])
    cluster_mapping = {o: n for o, n in zip(old_clusters, target_labels)}
    plot_clusters(curves=cluster_curves, cluster_colors=cluster_colors,
                  cluster_filter=old_clusters, cluster_mapping=cluster_mapping,
                  figsize=(10, 6), frequency_min=_frequency_min)


    # get_prediction_overview(regr, X,y,X_test)

    plot_feature_importance(regr, X_train, y_train, df_classify.drop(columns=["cluster_id"]).columns)
    # plot_confusion_matrix(regr, X_test, y_test)
    #tune(X,y)


if __name__ == "__main__":
    main()