# Charging Point Usage in Germany - Automated Retrieval,Analysis, and Usage Types explained

This repository contains the software and data used in the paper 
*Friese, P.A.; Michalk, W.; Fischer, M.; Hardt, C.; Bogenberger, K. Charging Point Usage in Germany—Automated Retrieval, Analysis, and Usage Types Explained. Sustainability 2021, 13, 13046. https://doi.org/10.3390/su132313046*.

The software was written in Python 3.9 and was developed and tested on a Linux `5.14` system. 

The Python requirements are given in `requirements.txt`, the most notable packages are given below:

- pandas (https://pandas.pydata.org/)
- numpy (https://numpy.org/)
- matplotlib (https://matplotlib.org/)
- sklearn (https://scikit-learn.org/stable/index.html)

## Dataset

The analysed dataset is given in `data/dataset.csv`. 
The file is `gzip`-compressed and semicolon-delimited.
It can be loaded e.g. using pandas via:
```python
import pandas as pd
df = pd.read_csv("data/dataset.csv",
                 sep=";",
                 compression="gzip", 
                 parse_dates=["state_start_ts","state_end_ts"])
df["interval_length"] = pd.to_timedelta(df.interval_length)
```

The dataset contains the following columns:

|Column             |Content                    |Data Type|
|:------------------|:--------------------------|:------|
| `interval_id`     | unique charge event ID     |  int |                                
| `evse_id`         | EVSE ID                    |  str |                               
| `operator_id`     | Operator ID                |  str |                               
| `state`           | Event Status               |  int |                                
| `state_start_ts`  | Timestamp of event start   |  datetime |
| `state_end_ts`    | Timestamp of event end     |  datetime |
| `interval_length` | Length of Event            |  timedelta |                     
| `lat`             | Charge Point Lat (WGS84)   |  float |                              
| `lon`             | Charge Point Lon (WGS84)   |  float |                              
| `power_rating_kw` | Power Rating in kW         |  float |                              
| `plug_type`       | Plug Type (Type 2, CCS,..) |  str |                               
| `power_type`      | Power Type (AC/DC)         |  str |      


The event `state` can be one of: 

- 0: unknown
- 1: available
- 2: occupied
- 3: defect

## Figure Overview

The following table gives an overview of the origin of all presented Figures:

| Figure | Content | Origin |
|:------|:-------------|:--------|
| Fig. **1** | Overview of charge points with bounding box        |  QGIS @ `data/qgis/qgis_project.qgz`
| Fig. **2**  | Visualisation of OCPP 3-tier model                 |  custom graphic
| Fig. **3**  | Overview two-phase aggregation                     |  custom graphic
| Fig. **4**  | Charge Length Histogram AC/DC                      |  `visualisers/plot_interval_lengths.py`
| Fig. **5**  | Charge Length Histogram AC, overnight              |  `visualisers/plot_interval_lengths.py`
| Fig. **6**  | Charge Length Histogram AC, anomaly closeup 1      |  `visualisers/plot_interval_lengths.py`
| Fig. **7**  | Charge Length Histogram AC, anomaly closeup 2      |  `visualisers/plot_interval_lengths.py`
| Fig. **8**  | Occupancy all AC/DC                                |  `visualisers/plot_timeofday_occupation.py`
| Fig. **9**  | Occupancy weekday AC/DC                            |  `visualisers/plot_timeofday_occupation.py`
| Fig. **10**  | Occupancy clusters                                 |  `visualisers/plot_occupation_clusters.py`
| Fig. **11** | Visualisation of weighted average approach         |  custom graphic
| Fig. **12** | Spatial distribution of predicted usage patterns   |  QGIS @ `data/qgis/qgis_project.qgz`
| Fig. **A1** | Charge Length Histogram AC/DC, anomaly whole range | `visualisers/plot_interval_lengths.py`
| Fig. **A2** | Geographic cells for socio-demographic data        |  not available
| Fig. **A3** | Occupancy curves for clusters 1 to 4               |  `visualisers/plot_occupation_clusters.py`
| Fig. **A4** | Spatial Distribution of predicted usage patterns with charge points |  QGIS @ `data/qgis/qgis_project.qgz`


## From download to figures

> Note: This software has been developed and tested on Linux and verified to work on macOS. 
> Windows support cannot be given, but is expected to work with slightly altered commands. 
> Windows 10 and upwards support the Windows Subsystem for Linux (WSL). This project has been tested on a WSL Debian 11 (Bullseye) installation.

### Python

##### Step 1
Install Python 3.9 either using the local package manager (apt, pacman, homebrew, etc.) or by following the install instructions at https://www.python.org/. 
Make sure that the version is at least 3.9. You can check the installed Python version using the command `python --version`.

##### Step 2
Download the contents from this repository and install the necessary requirements. The following commands assume a storage location at `./chargingpoints_code`
This should be done in a virtual environment such as [venv](https://docs.python.org/3/library/venv.html). You may have to install the Python module `venv` using your package manager.

You can create a virtual environment using e.g. `python3 -m venv venv_chargingpoints`.

Make sure the virtual environment is activated. On Linux/macOS this can be achieved by running `source ./venv_chargingpoints/bin/activate`. Depending on your shell, the activated venv may be indicated.

Assuming you have a working virtual environment (or use the regular Python environment), move to the repository code using `cd ./chargingpoints_code` and run `pip3 install -r requirements.txt` *from a shell at the project root* to install all requirements.

##### Step 3

Run the three visualisers to generate the Figures given in the paper:

- `PYTHONPATH=$(pwd) python3 visualisers/plot_interval_lengths.py`
- `PYTHONPATH=$(pwd) python3 visualisers/plot_timeofday_occupation.py`
- `PYTHONPATH=$(pwd) python3 visualisers/plot_occupation_clusters.py`

The prefix `PYTHONPATH=$(pwd)` sets the environment variable `PYTHONPATH` to the current working directory, which is the project root. 
Alternatively you can also directly set the current working directory using `PYTHONPATH=<path/to/projectroot> python3 visualisers/<file>.py`.

Using the standard configuration, this will generate Figures 3-9, Figure A1, and Figure A4. figures will be saved to folder `/tmp/images/`. 
The output path can be changed by passing the parameter `--outpath </path/to/folder>` to each visualiser, e.g. `PYTHONPATH=$(pwd) python3 visualisers/plot_interval_lengths.py --outpath /home/user/images/`

### QGIS

Figure 11, A2 and A5 have been generated usign [QGIS](https://www.qgis.org) 3.20. 
The QGIS project file is located at `data/qgis/qgis_project.qgz`. This project file contains the following layers:

- `bounding_box`, displaying the bounding box in which charge point status information has been gathered
- `all_charge_points`, displaying the charge point locations and power type (Figure A2)
- `sample_points_usage_pattern`, displaying the sample points and the predicted usage pattern
  - besides the predicted usage pattern, this layer also contains the probability for each of the four usage pattern in attributes `prob_cluster_X`
  - this layer is the basis for layer group `Cluster TINs` and `Cluster Meshes` (Figure A2, A5)
- `Cluster TINs` contains four layers, each representing the TIN interpolation result over layer `sample_points_usage_pattern`
  - the TIN interpolation is run on the respective cluster probability `prob_cluster_X`
- `Cluster Meshes` contains four layers similar to `Cluster TINs`, however the TIN interpolation is used to create a mesh (Figure A2,A5)
- `charge_points_with_usage_patterns` displays the charge points used for training the Random Forest Classifier and their *true usage patterns* (and *not* the predicted usage pattern)
- `cluster_df` is a data frame containing the cluster ID for all charge points
  - this data source is joined with `charge_points_with_usage_patterns` to visualise the true cluster labels

The background tiles originate from OpenStreetMap, © OpenStreetMap contributors. 
See https://www.openstreetmap.org/copyright for more information on the copyright and license.

### Custom Graphics

Figures 1, 2, and 10 have been created manually. These images are located in `data/images`.

The data for Figure 3 cannot be shared due to copyright constraints of the used data set.
