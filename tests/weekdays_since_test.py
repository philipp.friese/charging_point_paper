import unittest
import datetime
import numpy as np
import pytz
from algorithms.weekdays_since import weekdays_since

class TestWeekdaysSince(unittest.TestCase):
    def test_hardcoded(self):
        dt_start = datetime.datetime(2021,5,3)
        dt_end = datetime.datetime(2021,8,4)

        expected = [14,14,13,13,13,13,13]
        actual = weekdays_since(dt_start, dt_end)
        self.assertListEqual(expected, actual)


    def test_hardcoded_tz(self):
        dt_start = pytz.timezone("Europe/Berlin").localize(datetime.datetime(2021,5,3))
        dt_end = pytz.timezone("Europe/Berlin").localize(datetime.datetime(2021,8,4))

        expected = [14,14,13,13,13,13,13]
        actual = weekdays_since(dt_start, dt_end)
        self.assertListEqual(expected, actual)

    def test_hardcoded_tz_mixed(self):
        dt_start = pytz.timezone("Europe/Berlin").localize(datetime.datetime(2021,5,3))
        # this is 2021-08-04T02:00+02:00, so the Wednesday has proceeded a bit in Europe/Berlin time, but that doesn't change things
        dt_end = pytz.utc.localize(datetime.datetime(2021,8,4))

        expected = [14,14,13,13,13,13,13]
        actual = weekdays_since(dt_start, dt_end)
        self.assertListEqual(expected, actual)


        dt_start = pytz.utc.localize(datetime.datetime(2021, 5, 3))
        # this will be 2021-08-03T22:00 in UTC, so this UTC-Tuesday is not yet complete, so don't count is as a full day
        dt_end = pytz.timezone("Europe/Berlin").localize(datetime.datetime(2021, 8, 4))

        expected = [14, 13, 13, 13, 13, 13, 13]
        actual = weekdays_since(dt_start, dt_end)
        self.assertListEqual(expected, actual)


    def test_range(self):
        dt_start = datetime.datetime(2021, 5, 3) # monday
        dt_end_initial = dt_start
        expected = np.array([0, 0, 0, 0, 0, 0, 0]) # we start at dt_start, so no days have passed
        # for a bit over two weeks, keep incrementing our current "now"; so for eg dt_start+1d, we expect that one monday has passed (dt_start)
        # so also increment the expected counter by 1 at index 0 (which maps to monday)
        for i, dt_end in [(i, dt_end_initial + datetime.timedelta(days=i)) for i in range(15)]:
            actual = weekdays_since(dt_start, dt_end)
            self.assertListEqual(list(expected), actual)
            expected[i % 7] += 1 # bump counter for next iteration
if __name__ == '__main__':
    unittest.main()
