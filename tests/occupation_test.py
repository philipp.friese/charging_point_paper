import unittest

import pandas as pd
import datetime
import bisect
from algorithms.timeframe_raster import raster_timeframes

pd.options.display.width = 1920
pd.options.display.max_columns = 144
pd.options.display.max_rows = 144
pd.options.display.min_rows = 144


def _get_probe_values(probe_points: list[datetime.time],
                      timeframes: list[tuple[datetime.datetime,datetime.datetime]],
                      resolution_min: int = 1) -> dict[datetime.time,list[int]]:
    """
    For a given list of probe points (e.g. at 12:40, 13:00, 13:20, 13:40, ...), calculate the number of timeframes (from start_time to end_time) overlapping it.

    This implementation includes a resolution: For a resolution of e.g. 30 minutes, convert time frame markers into "buckets":
     Buckets are 00:00, 00:30, 01:00, 01:30,...
     For start_time:=12:40, end_time:=13:25:
      start_time -> 12:30 (round down; 12:40 falls into the "bucket" of [12:30,13:00), so pick 12:30 as start)
      end_time   -> 13:30 (round up  ; 13:30 falls into the "bucket" of [13:00,13:30), so pick 13:30 as end)

    This algorithm asserts one core property: time frames must be shorter than (24h - 2*resolution).
    For a resolution of 10 minutes: the maximum timeframe must be shorter than 23h40min, for 1min resolution: 23h58min.
    Reason: This algorithm is not (yet) designed to handle multi-day time frames.
     The offset of 2*resolution is related to the "bucketing" approach: e.g. if a timeframe is 23h55min long and we have 10min buckets (total of 144 buckets),
     then we will still exceed 144 buckets, which effectively overflows the system.

    :param probe_points: time points to sample for
    :param timeframes: time frames (list of start and end markers) to check for
    :param resolution_min: resolution in minutes
    :return: Dictionary with format {probe_point:list[matching timeframe indices]}
    """
    num_slices = (60*24)//resolution_min
    samples = [(datetime.datetime(2021,1,1) + datetime.timedelta(minutes=i*resolution_min)).time()
               for i in range(num_slices)]

    probe_values = {}
    for time in probe_points:
        time: datetime.time

        _is = []
        for i, (start_dtime, end_dtime) in enumerate(timeframes):
            assert end_dtime - start_dtime < (datetime.timedelta(days=1) - datetime.timedelta(minutes=resolution_min*2))
            start_time = samples[bisect.bisect(samples, start_dtime.time())-1]
            end_time = samples[bisect.bisect(samples, end_dtime.time())-1]

            if start_time <= end_time:
                if start_time <= time < end_time:
                    _is.append(i)
            else:
                if (start_time <= time and end_time < time) or \
                   (start_time >= time and end_time > time):
                    _is.append(i)

        probe_values[time] = _is
    return probe_values


def _generate_test_set_simple(timeframes: list[tuple[datetime.datetime, datetime.datetime]],
                              resolution_min: int) -> tuple[pd.DataFrame, pd.DataFrame]:
    assert (60 * 24) // resolution_min * resolution_min == 1440, "resolution_min must tile 1440min"
    probe_points = [
        (datetime.datetime(2021, 1, 1, 0, 0) + datetime.timedelta(minutes=i * resolution_min)).time()
        for i in range(60 * 24 // resolution_min)]

    probes = _get_probe_values(probe_points, timeframes, resolution_min)
    probes_df = pd.DataFrame([[i, v, int(len(v))] for i, v in probes.items()],
                             columns=["probe_time", "matches", "count"]).set_index("probe_time")
    test_df = pd.DataFrame(timeframes, columns=["start_ts", "end_ts"])
    return test_df, probes_df

class ProbeTest(unittest.TestCase):
    def test_singular(self):
        timeframe = [(datetime.datetime(2021,1,1, 12,0), datetime.datetime(2021,1,1, 13, 0))]
        probe = [datetime.time(12,30)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe)[probe[0]]))

        probe = [datetime.time(11, 30)]
        self.assertEqual(0, len(_get_probe_values(probe, timeframe)[probe[0]]))

    def test_singular_overnight(self):
        timeframe = [(datetime.datetime(2021, 1, 1, 18, 0), datetime.datetime(2021, 1, 2, 8, 0))]
        probe = [datetime.time(12, 30)]
        self.assertEqual(0, len(_get_probe_values(probe, timeframe)[probe[0]]))

        probe = [datetime.time(19, 30)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe)[probe[0]]))

        probe = [datetime.time(6, 30)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe)[probe[0]]))

    def test_multiple(self):
        timeframe = [(datetime.datetime(2021, 1, 1, 12, 0), datetime.datetime(2021, 1, 1, 13, 0)),
                     (datetime.datetime(2021, 1, 1, 10, 0), datetime.datetime(2021, 1, 1, 13, 0)),
                     (datetime.datetime(2021, 1, 1, 16, 0), datetime.datetime(2021, 1, 1, 18, 0))]
        probe = [datetime.time(12, 30)]
        self.assertEqual(2, len(_get_probe_values(probe, timeframe,1)[probe[0]]))

        probe = [datetime.time(11, 30)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe,1)[probe[0]]))

    def test_multiple_overnight(self):
        timeframe = [(datetime.datetime(2021, 1, 1, 18, 0), datetime.datetime(2021, 1, 2, 8, 0)),
                     (datetime.datetime(2021, 1, 1, 16, 0), datetime.datetime(2021, 1, 2, 4, 0)),
                     (datetime.datetime(2021, 1, 1, 20, 0), datetime.datetime(2021, 1, 2, 10, 0))]
        probe = [datetime.time(12, 0)]
        self.assertEqual(0, len(_get_probe_values(probe, timeframe)[probe[0]]))

        probe = [datetime.time(19, 30)]
        self.assertEqual(2, len(_get_probe_values(probe, timeframe)[probe[0]]))
        probe = [datetime.time(17, 30)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe)[probe[0]]))

        probe = [datetime.time(9, 0)]
        self.assertEqual(1, len(_get_probe_values(probe, timeframe)[probe[0]]))
        probe = [datetime.time(3, 0)]
        self.assertEqual(3, len(_get_probe_values(probe, timeframe)[probe[0]]))
        probe = [datetime.time(5, 0)]
        self.assertEqual(2, len(_get_probe_values(probe, timeframe)[probe[0]]))


class OccupationSimple(unittest.TestCase):

    def testValidTimeframes(self):
        timeframes = [
            (datetime.datetime(2021, 1, 1, 10,15), datetime.datetime(2021, 1, 1, 12, 1)),  # 0
            (datetime.datetime(2021, 1, 1, 11, 0), datetime.datetime(2021, 1, 1, 13, 0)),  # 1
            (datetime.datetime(2021, 1, 1, 6, 0), datetime.datetime(2021, 1, 1, 7, 30)),  # 2
            (datetime.datetime(2021, 1, 1, 6, 30), datetime.datetime(2021, 1, 1, 7, 42)),  # 3
            (datetime.datetime(2021, 1, 1, 12, 59), datetime.datetime(2021, 1, 1, 14, 8)),  # 4
            (datetime.datetime(2021, 1, 1, 12, 59), datetime.datetime(2021, 1, 1, 14, 8)),  # 5
            (datetime.datetime(2021, 1, 1, 13, 30), datetime.datetime(2021, 1, 1, 16, 15)),  # 6
            (datetime.datetime(2021, 1, 1, 18, 30), datetime.datetime(2021, 1, 1, 22, 0)),  # 7
            (datetime.datetime(2021, 1, 1, 22, 30), datetime.datetime(2021, 1, 2, 5, 0)),  # 8 wraps!
            (datetime.datetime(2021, 1, 1, 22, 45), datetime.datetime(2021, 1, 2, 8, 0)),  # 9 wraps!
            (datetime.datetime(2020, 12, 31, 22, 45), datetime.datetime(2021, 1, 1, 8, 0)),  # 10 wraps!
            (datetime.datetime(2021, 1, 1, 18, 30,20), datetime.datetime(2021, 1, 1, 22, 0,11)),  # 11
            (datetime.datetime(2021, 1, 1, 22, 30,42), datetime.datetime(2021, 1, 2, 5, 0,22)),  # 12 wraps!
        ]
        test_df, probes_df = _generate_test_set_simple(timeframes, 10)
        raster_df = raster_timeframes(test_df, resolution_min=10)
        pd.testing.assert_series_equal(probes_df["count"], raster_df["count"], check_names=False, check_dtype=False)

        test_df, probes_df = _generate_test_set_simple(timeframes, 1)
        raster_df = raster_timeframes(test_df, resolution_min=1)
        pd.testing.assert_series_equal(probes_df["count"], raster_df["count"], check_names=False, check_dtype=False)

        test_df, probes_df = _generate_test_set_simple(timeframes, 30)
        raster_df = raster_timeframes(test_df, resolution_min=30)
        pd.testing.assert_series_equal(probes_df["count"], raster_df["count"], check_names=False, check_dtype=False)


    def testInvalidResolution(self):
        # this resolution does not tile 1440minutes: expect error
        self.assertRaises(AssertionError, _generate_test_set_simple, [], 42)
        self.assertRaises(AssertionError, raster_timeframes, None, 42)


    def testInvalidTimeframes(self):
        timeframes = [(datetime.datetime(2021, 1, 1, 10, 0), datetime.datetime(2021, 1, 1, 12, 0)),   # 0 valid
                      (datetime.datetime(2021, 1, 1, 12, 0), datetime.datetime(2021, 1, 2, 23, 0)),   # 1 invalid; wraps "too far"
                      (datetime.datetime(2021, 1, 1, 12, 0), datetime.datetime(2021, 1, 2, 11, 59))]  # 2 invalid; wraps "too far"
        test_df = pd.DataFrame(timeframes, columns=["start_ts", "end_ts"])
        self.assertRaises(AssertionError, _generate_test_set_simple, timeframes, 1)
        self.assertRaises(AssertionError, raster_timeframes, test_df, 1)

        # test that larger gap between two timeframe markers is valid again
        timeframes = [(datetime.datetime(2021, 1, 1, 10, 0), datetime.datetime(2021, 1, 1, 12, 0)),   # 0
                      (datetime.datetime(2021, 1, 1, 12, 0), datetime.datetime(2021, 1, 2, 11, 57))]  # 1
        test_df = pd.DataFrame(timeframes, columns=["start_ts", "end_ts"])
        self.assertIsInstance(raster_timeframes(test_df, 1), pd.DataFrame)

def main():
    timeframes = [
        (datetime.datetime(2021, 1, 1, 10, 15), datetime.datetime(2021, 1, 1, 12, 1)),  # 0
        (datetime.datetime(2021, 1, 1, 11, 0), datetime.datetime(2021, 1, 1, 13, 0)),  # 1
        (datetime.datetime(2021, 1, 1, 6, 0), datetime.datetime(2021, 1, 1, 7, 30)),  # 2
        (datetime.datetime(2021, 1, 1, 6, 30), datetime.datetime(2021, 1, 1, 7, 42)),  # 3
        (datetime.datetime(2021, 1, 1, 12, 59), datetime.datetime(2021, 1, 1, 14, 8)),  # 4
        (datetime.datetime(2021, 1, 1, 12, 59), datetime.datetime(2021, 1, 1, 14, 8)),  # 5
        (datetime.datetime(2021, 1, 1, 13, 30), datetime.datetime(2021, 1, 1, 16, 15)),  # 6
        (datetime.datetime(2021, 1, 1, 18, 30), datetime.datetime(2021, 1, 1, 22, 0)),  # 7
        (datetime.datetime(2021, 1, 1, 22, 30), datetime.datetime(2021, 1, 2, 5, 0)),  # 8 wraps!
        (datetime.datetime(2021, 1, 1, 22, 45), datetime.datetime(2021, 1, 2, 8, 0)),  # 9 wraps!
        (datetime.datetime(2020, 12, 31, 22, 45), datetime.datetime(2021, 1, 1, 8, 0)),  # 10 wraps!
    ]
    resolution_min = 1

    assert (60 * 24) // resolution_min * resolution_min == 1440, "resolution_min must tile 1440min"
    probe_points = [
        (datetime.datetime(2021, 1, 1, 0, 0) + datetime.timedelta(minutes=i * resolution_min)).time()
        for i in range(60 * 24 // resolution_min)]

    probes = _get_probe_values(probe_points, timeframes, resolution_min)
    probes_df = pd.DataFrame([[i, v, len(v)] for i, v in probes.items()],
                             columns=["probe_time", "matches", "count"]).set_index("probe_time")
    probes_df["count"] = pd.to_numeric(probes_df["count"], downcast="signed")
    test_df = pd.DataFrame(timeframes, columns=["start_ts", "end_ts"])
    raster_df = raster_timeframes(test_df, resolution_min=10)
    unittest.main()

if __name__ == '__main__':
    main()
